import pytest

from validation_workflow.operators.transformations.utils import (
    inspect_function_params,
    get_transform_params,
    cast_transformation_params,
)
from validation_workflow.utils import describe


@pytest.fixture
def the_test_func():
    @describe(name="Transform testing", category="Special")
    def transform_testing(accessor, a, b: float, c: str = "Linda"):
        assert (accessor, a, b, c)

    return transform_testing


def test_inspect_function_params(the_test_func):
    expected_params = [
        {"name": "a", "default": None, "type": str},
        {"name": "b", "default": None, "type": float},
        {"name": "c", "default": "Linda", "type": str},
    ]
    assert inspect_function_params(the_test_func) == expected_params


def test_get_transform_params():
    expected_transform_params = {
        "id": "nop",
        "name": "Identity",
        "category": "Basic",
        "description": "Identity",
        "params": [],
    }
    assert get_transform_params("nop") == expected_transform_params


@pytest.mark.parametrize(
    "tr_func_name,params,expected_params",
    [
        ("scale", {"value": "2", "qcode": "1"}, {"value": 2.0, "qcode": 1}),
        ("scale", {"value": "2"}, {"value": 2.0, "qcode": 1}),
        ("scale", {"value": "3.14", "qcode": "1"}, {"value": 3.14, "qcode": 1}),
        (
            "compute",
            {"cmd": "foo", "additional": "bar"},
            {"cmd": "foo", "additional": "bar"},
        ),
    ],
)
def test_cast_transformation_params(tr_func_name, params, expected_params):
    assert cast_transformation_params(tr_func_name, params) == expected_params
