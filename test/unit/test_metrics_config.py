import os
from datetime import datetime, timedelta

from validation_workflow.metrics_config import (
    MetricsConfig,
    LocationConfig,
    LocationConfigDagArgs,
    MetricConfig,
)


def test_load_metrics_config():
    path = os.path.join(os.path.dirname(__file__), "../files/test_config.yaml")
    metrics_config = MetricsConfig()
    metrics_config.load_from_file(path)

    assert set(metrics_config.get_locations()) == {"demo_1", "demo_2"}

    location_config_1 = metrics_config.get_location_config("demo_1")
    expected_location_config_1 = LocationConfig(
        name="Demo.1",
        source_format="csv",
        source=["demo/1/{{ ds_nodash }}.txt"],
        dag_args=LocationConfigDagArgs(
            start_date=datetime(2020, 1, 1),
            retry_delay=timedelta(minutes=5),
            retries=3,
            schedule_interval=timedelta(days=1),
        ),
        metrics=[
            MetricConfig(
                name_raw="Temp@1",
                name_unit="Temperature 10% (%)",
                name_final="Temperature 10% (%)",
                transformation="scale",
                transformation_params={"coefficient": 100},
            ),
            MetricConfig(
                name_raw="CG",
                name_unit="CG (W*m-2)",
                name_final=None,
                transformation="compute",
                transformation_params={
                    "cmd": "{CG} / {c}*1000 + 5.67 * 10**-8 * ({Temp@1}+273.15)**4",
                    "c": 12.5,
                },
            ),
        ],
        frequency=timedelta(minutes=10),
    )
    assert location_config_1 == expected_location_config_1

    location_config_2 = metrics_config.get_location_config("demo_2")
    expected_location_config_2 = LocationConfig(
        name="Demo.2",
        source_format="csv",
        source=["demo/2/{{ ds_nodash }}.txt"],
        dag_args=LocationConfigDagArgs(
            start_date=datetime(2020, 1, 1, 12, 0, 0),
            retry_delay=timedelta(minutes=10),
            retries=6,
            schedule_interval=timedelta(days=7),
        ),
        metrics=[
            MetricConfig(
                name_raw="A",
                name_unit="A",
                name_final="A",
                transformation="nop",
                transformation_params={},
            )
        ],
        frequency=timedelta(hours=1),
    )
    assert location_config_2 == expected_location_config_2
