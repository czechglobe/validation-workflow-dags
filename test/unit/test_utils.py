import datetime

import pendulum
from pandas._libs.tslibs.timestamps import Timestamp

from validation_workflow.utils import (
    idealize,
    iso_timestamp,
    iso_string,
    get_timestamps,
)


def test_idealize():
    assert idealize("abc") == "abc"
    assert idealize(".2*_#") == "_2-_-"
    assert idealize("Strážkovice") == "strazkovice"


def test_iso_timestamp():
    assert iso_timestamp("2020-01-01") == Timestamp(
        "2020-01-01 00:00:00+0000", tz="UTC"
    )
    assert iso_timestamp("2020-01-01T00:00:00") == Timestamp(
        "2020-01-01 00:00:00+0000", tz="UTC"
    )
    assert iso_timestamp(datetime.datetime(2020, 1, 1)) == Timestamp(
        "2020-01-01 00:00:00+0000", tz="UTC"
    )
    assert iso_timestamp(pendulum.datetime(2020, 1, 1)) == Timestamp(
        "2020-01-01 00:00:00+0000", tz="UTC"
    )


def test_iso_string():
    assert iso_string(datetime.datetime(2020, 1, 1)) == "2020-01-01T00:00:00"
    assert iso_string(pendulum.datetime(2020, 1, 1)) == "2020-01-01T00:00:00"


def test_get_timestamps():
    expected_timestams = [
        iso_timestamp("2020-01-01T04:00:00"),
        iso_timestamp("2020-01-01T05:00:00"),
        iso_timestamp("2020-01-01T06:00:00"),
    ]
    timestamps = get_timestamps(
        iso_timestamp("2020-01-01T04:00:00"),
        iso_timestamp("2020-01-01T07:00:00"),
        reindex_frequency=datetime.timedelta(hours=1),
    )
    assert set(timestamps) == set(expected_timestams)
