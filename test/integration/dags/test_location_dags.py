from airflow.models import DagBag
from validation_workflow.metrics_config import load_metrics_config


def test_dags_list():
    dag_bag = DagBag()

    metrics_config = load_metrics_config()
    dag_names = set(dag_bag.dag_ids)
    expected_dag_names = {l for l in metrics_config.get_locations()}

    for expected_dag_name in expected_dag_names:
        assert expected_dag_name in dag_names
