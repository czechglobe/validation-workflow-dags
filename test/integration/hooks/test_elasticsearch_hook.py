import pandas as pd
import pytest

from airflow import settings
from airflow.models import Connection
from airflow.utils.db import merge_conn
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.utils import iso_timestamp


class TestElasticsearchHook:
    index_definition = {
        "mappings": {
            "properties": {
                "@timestamp": {"type": "date"},
                "location": {
                    "type": "text",
                    "fields": {"keyword": {"type": "keyword", "ignore_above": 256}},
                },
                "qcode": {"type": "integer"},
            }
        }
    }

    def get_es_hook(self):
        session = settings.Session()
        merge_conn(
            Connection(
                conn_id="elasticsearch_test",
                conn_type="http",
                host="test-elasticsearch",
                port=9200,
            ),
            session,
        )
        return ElasticsearchHook("elasticsearch_test")

    def test_create_delete_index(self):
        es_hook = self.get_es_hook()
        if es_hook.es.indices.exists("room"):
            es_hook.delete_index("room")

        assert not es_hook.es.indices.exists("room")

        es_hook.create_index("room", self.index_definition)

        assert es_hook.es.indices.exists("room")

        es_hook.delete_index("room")

        assert not es_hook.es.indices.exists("room")

    def test_time_series_empty(self):
        es_hook = self.get_es_hook()
        es_hook.create_index("room", self.index_definition)

        time_series = es_hook.load_time_series(
            "room", "TorrentDePareis", "temperature", "2020-03-13", "2020-03-14"
        )

        assert time_series.shape == (0, 2)
        assert set(time_series.columns) == {"value", "qcode"}

    def test_time_series_wrong_df(self):
        es_hook = self.get_es_hook()

        common = ["room", "TorrentDePareis", "temperature"]

        with pytest.raises(ValueError):
            es_hook.store_time_series(*common, pd.Series())

        with pytest.raises(ValueError):
            es_hook.store_time_series(*common, pd.DataFrame())

        with pytest.raises(ValueError):
            es_hook.store_time_series(*common, pd.DataFrame(columns=["value"]))
