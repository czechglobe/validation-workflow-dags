import portion as P

from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.utils import iso_timestamp


class TestElasticsearchIntervalStorageHook:
    day1_full = P.closedopen(iso_timestamp("2020-03-13"), iso_timestamp("2020-03-14"))
    day1_gap = P.closedopen(
        iso_timestamp("2020-03-13T12:00:00"), iso_timestamp("2020-03-14T14:00:00")
    )
    day1_with_gap = day1_full - day1_gap
    day2_full = P.closedopen(iso_timestamp("2020-03-14"), iso_timestamp("2020-03-15"))
    day2_gap = P.closedopen(
        iso_timestamp("2020-03-14T10:00:00"), iso_timestamp("2020-03-15T16:00:00")
    )
    day2_with_gap = day2_full - day2_gap

    def test_default(self, es_interval_storage_hook: ElasticsearchIntervalStorageHook):
        load_intervals = es_interval_storage_hook.load_intervals(
            ".intervals-config", "uno", 4
        )

        expected_intervals = P.IntervalDict()
        expected_intervals[P.open(-P.inf, P.inf)] = 4

        assert load_intervals == expected_intervals

    def test_intervals(
        self, es_interval_storage_hook: ElasticsearchIntervalStorageHook
    ):
        intervals = P.IntervalDict()
        intervals[self.day1_full] = True

        es_interval_storage_hook.store_intervals(".intervals-config", "uno", intervals)
        load_intervals = es_interval_storage_hook.load_intervals(
            ".intervals-config", "uno", False
        )

        expected_intervals = P.IntervalDict()
        expected_intervals[P.open(-P.inf, P.inf)] = False
        expected_intervals[self.day1_full] = True

        assert load_intervals == expected_intervals

    def test_intervals_complex_value(
        self, es_interval_storage_hook: ElasticsearchIntervalStorageHook
    ):
        expected_intervals = P.IntervalDict()
        expected_intervals[self.day1_full] = {"complex": {"stored": ["object"]}}

        es_interval_storage_hook.store_intervals(
            ".intervals-config", "uno", expected_intervals
        )
        load_intervals = es_interval_storage_hook.load_intervals(
            ".intervals-config", "uno"
        )

        assert load_intervals == expected_intervals

    def test_multiple_intervals(
        self, es_interval_storage_hook: ElasticsearchIntervalStorageHook
    ):
        intervals1 = P.IntervalDict()
        intervals1[self.day1_full] = 2
        intervals1[self.day1_with_gap] = 3

        intervals2 = P.IntervalDict()
        intervals2[self.day2_full] = 2
        intervals2[self.day2_with_gap] = 3

        es_interval_storage_hook.store_multiple_intervals(
            ".intervals-config", {"uno": intervals1, "dos": intervals2}
        )
        load_multiple_intervals = es_interval_storage_hook.load_multiple_intervals(
            ".intervals-config", ["uno", "dos"]
        )

        assert load_multiple_intervals["uno"] == intervals1
        assert load_multiple_intervals["dos"] == intervals2

    def test_merge_intervals_with_prefix(
        self, es_interval_storage_hook: ElasticsearchIntervalStorageHook
    ):
        intervals1 = P.IntervalDict()
        intervals1[self.day1_full] = 2
        intervals1[self.day1_with_gap] = 3

        intervals2 = P.IntervalDict()
        intervals2[self.day2_full] = 2
        intervals2[self.day2_with_gap] = 3

        es_interval_storage_hook.store_multiple_intervals(
            ".intervals-config", {"magic::uno": intervals1, "magic::dos": intervals2}
        )
        load_merge_intervals_with_prefix = es_interval_storage_hook.load_merge_intervals_with_prefix(
            ".intervals-config", "magic::"
        )

        expected_intervals = P.IntervalDict()
        expected_intervals.update(intervals1)
        expected_intervals.update(intervals2)

        assert load_merge_intervals_with_prefix == expected_intervals

    def test_merge_intervals_with_prefix_default(
        self, es_interval_storage_hook: ElasticsearchIntervalStorageHook
    ):
        intervals1 = P.IntervalDict()
        intervals1[self.day1_full] = 2
        intervals1[self.day1_with_gap] = 3

        intervals2 = P.IntervalDict()
        intervals2[self.day2_full] = 2
        intervals2[self.day2_with_gap] = 3

        es_interval_storage_hook.store_multiple_intervals(
            ".intervals-config", {"magic::uno": intervals1, "magic::dos": intervals2}
        )
        load_merge_intervals_with_prefix = es_interval_storage_hook.load_merge_intervals_with_prefix(
            ".intervals-config", "magic::", 1
        )

        expected_intervals = P.IntervalDict()
        expected_intervals[P.open(-P.inf, P.inf)] = 1
        expected_intervals.update(intervals1)
        expected_intervals.update(intervals2)

        assert load_merge_intervals_with_prefix == expected_intervals
