import json
import logging

import pytest

from airflow import settings
from airflow.models import Connection
from airflow.utils.db import merge_conn
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.hooks.find_fs_hook import FindFSHook


def pytest_configure(config):
    if config.getoption("verbose") == 0:
        logger = logging.getLogger("airflow")
        logger.propagate = False
        logger = logging.getLogger("elasticsearch")
        logger.propagate = False


@pytest.fixture
def es_hook():
    session = settings.Session()
    merge_conn(
        Connection(
            conn_id="elasticsearch_test",
            conn_type="http",
            host="test-elasticsearch",
            port=9200,
        ),
        session,
    )
    es_hook = ElasticsearchHook("elasticsearch_test")
    es_hook.create_index(
        "raw",
        {
            "mappings": {
                "properties": {
                    "@timestamp": {"type": "date"},
                    "location": {
                        "type": "text",
                        "fields": {"keyword": {"type": "keyword", "ignore_above": 256}},
                    },
                    "qcode": {"type": "integer"},
                }
            }
        },
    )
    yield es_hook
    es_hook.delete_index("raw")


@pytest.fixture
def es_interval_storage_hook():
    session = settings.Session()
    merge_conn(
        Connection(
            conn_id="elasticsearch_test",
            conn_type="http",
            host="test-elasticsearch",
            port=9200,
        ),
        session,
    )
    es_hook = ElasticsearchHook("elasticsearch_test")
    es_hook.create_index(
        ".intervals-config",
        {
            "mappings": {
                "properties": {
                    "fragments": {"type": "object", "enabled": False},
                    "key": {"type": "text", "analyzer": "keyword"},
                }
            }
        },
    )
    es_interval_storage_hook = ElasticsearchIntervalStorageHook("elasticsearch_test")
    yield es_interval_storage_hook
    es_hook.delete_index(".intervals-config")


@pytest.fixture
def find_fs_hook():
    session = settings.Session()
    merge_conn(
        Connection(
            conn_id="fs_test",
            conn_type="fs",
            extra=json.dumps({"path": "/var/data-test"}),
        ),
        session,
    )
    return FindFSHook(conn_id="fs_test")
