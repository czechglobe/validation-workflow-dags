from datetime import timedelta

import numpy as np
import pendulum
import portion as P

from test.utils import generate_time_series, assert_dataframe_equals
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.find_fs_hook import FindFSHook
from validation_workflow.operators.interval_accessor import IntervalAccessor
from validation_workflow.operators.time_series_file_elastic_transfer import (
    TimeSeriesFileElasticTransfer,
)
from validation_workflow.utils import iso_timestamp


class TestTimeSeriesFileElasticTransfer:
    def test_execute_tsv(self, es_hook: ElasticsearchHook, find_fs_hook: FindFSHook):
        operator = TimeSeriesFileElasticTransfer(
            task_id="test_task",
            location="TorrentDePareis",
            file_format="tsv",
            fs_conn_id=find_fs_hook.conn.conn_id,
            elasticsearch_conn_id=es_hook.elasticsearch_conn_id,
            filepath="test_input_2020-01-01_*.tsv",
            reindex_frequency=timedelta(hours=1),
            columns=["temp10", "hum10", "ws10"],
        )

        context = {
            "execution_date": pendulum.datetime(2020, 1, 1, 0, 0, 0),
            "next_execution_date": pendulum.datetime(2020, 1, 2, 0, 0, 0),
        }

        operator.execute(context)

        accessors = {
            metric: IntervalAccessor(
                es_hook,
                src_index="raw",
                dst_index="raw",
                location="TorrentDePareis",
                src_name=metric,
                dst_name=metric,
                reindex_frequency=timedelta(hours=1),
                process_interval=P.closedopen(
                    iso_timestamp("2020-01-01"), iso_timestamp("2020-01-02")
                ),
            )
            for metric in ["temp10", "hum10", "ws10", "ignored"]
        }

        time_series = accessors["temp10"].get_time_series()
        expected_time_series = generate_time_series(
            [17.0, 17.0, np.nan, 16.0] + [np.nan] * 20, 0, since="2020-01-01"
        )
        assert_dataframe_equals(time_series, expected_time_series)

        time_series = accessors["hum10"].get_time_series()
        expected_time_series = generate_time_series(
            [0.4, 0.5, np.nan, 0.6] + [np.nan] * 20, 0, since="2020-01-01"
        )
        assert_dataframe_equals(time_series, expected_time_series)

        time_series = accessors["ws10"].get_time_series()
        expected_time_series = generate_time_series(
            [np.nan, 0.23, 0.25] + [np.nan] * 21, 0, since="2020-01-01"
        )
        assert_dataframe_equals(time_series, expected_time_series)

        time_series = accessors["ignored"].get_time_series()
        expected_time_series = generate_time_series(
            [np.nan] * 24, 0, since="2020-01-01"
        )
        assert_dataframe_equals(time_series, expected_time_series)
