import numpy as np
import pytest

from test.utils import assert_dataframe_equals, generate_time_series
from validation_workflow.operators.interval_accessor import (
    IntervalAccessor,
    GapsAccessor,
)
from validation_workflow.operators.transformations.missing_values import (
    linear_interpolation,
    polynomial_interpolation,
    transplant,
)


@pytest.mark.parametrize(
    "tr_func,params,expected",
    [
        (linear_interpolation, {"max_gap": "1 day", "qcode": 1}, [1, 1, 2, 3, 4]),
        (
            polynomial_interpolation,
            {"max_gap": "1 day", "qcode": 1},
            [1, 1, 1.5, 2.5, 4],
        ),
    ],
)
def test_transformation_missing_values(
    tr_func, params, expected, accessor: IntervalAccessor, gaps_accessor: GapsAccessor
):
    time_series = generate_time_series([1, 1, np.nan, np.nan, 4])
    accessor.set_time_series(time_series)

    tr_func(accessor, gaps_accessor, **params)

    time_series = accessor.get_time_series()
    expected_time_series = generate_time_series(expected)
    assert_dataframe_equals(time_series, expected_time_series)


def test_transformation_transplant(
    accessor: IntervalAccessor, gaps_accessor: GapsAccessor
):
    time_series = generate_time_series([1, 1, np.nan, np.nan, 4])
    accessor.set_time_series(time_series)

    accessor_a = accessor.subaccessor(src_name="a", dst_name="a")
    accessor_a.set_time_series(generate_time_series([6, 7, 8, 9, 10]))

    transplant(
        accessor,
        gaps_accessor,
        src_location=accessor_a.location,
        src_name=accessor_a.dst_name,
        time_shift="0 minutes",
        time_scale=1.0,
        value_scale=1.0,
        qcode=1,
    )

    time_series = accessor.get_time_series()
    expected_time_series = generate_time_series([1, 2, 8, 9, 5])
    assert_dataframe_equals(time_series, expected_time_series)
