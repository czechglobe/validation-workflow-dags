import numpy as np
import pytest

from test.utils import assert_dataframe_equals, generate_time_series
from validation_workflow.operators.interval_accessor import (
    IntervalAccessor,
    GapsAccessor,
)
from validation_workflow.operators.transformations.basic import (
    nop,
    scale,
    shift,
    constant,
    remove,
    scaleshift,
)


@pytest.mark.parametrize(
    "tr_func,params,expected",
    [
        (nop, {}, [0, 1, 2, 3, 4]),
        (scale, {"value": 2.0, "qcode": 1}, [0, 2, 4, 6, 8]),
        (shift, {"value": 2.0, "qcode": 1}, [2, 3, 4, 5, 6]),
        (constant, {"value": 2.0, "qcode": 1}, [2, 2, 2, 2, 2]),
        (remove, {}, [np.nan] * 5),
        (scaleshift, {"a": 2, "b": 1, "qcode": 1}, [1, 3, 5, 7, 9]),
    ],
)
def test_transformation_basic(
    tr_func, params, expected, accessor: IntervalAccessor, gaps_accessor: GapsAccessor
):
    time_series = generate_time_series([0, 1, 2, 3, 4])
    accessor.set_time_series(time_series)

    tr_func(accessor, gaps_accessor, **params)

    time_series = accessor.get_time_series()
    expected_time_series = generate_time_series(expected)

    assert_dataframe_equals(time_series, expected_time_series)


def test_qcode(accessor: IntervalAccessor, gaps_accessor: GapsAccessor):
    time_series = generate_time_series([0, 1, 2, 3, 4], 1)

    accessor.set_time_series(time_series)

    scale(accessor, gaps_accessor, qcode=2)

    time_series = accessor.get_time_series()
    expected_time_series = generate_time_series([0, 1, 2, 3, 4], 2)
    assert_dataframe_equals(time_series, expected_time_series)
