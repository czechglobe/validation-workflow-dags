from datetime import timedelta

import portion as P
import pytest

from airflow import settings
from airflow.models import Connection
from airflow.utils.db import merge_conn
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.operators.interval_accessor import (
    IntervalAccessor,
    GapsAccessor,
)
from validation_workflow.utils import iso_timestamp


@pytest.fixture
def accessor():
    session = settings.Session()
    merge_conn(
        Connection(
            conn_id="elasticsearch_test",
            conn_type="http",
            host="test-elasticsearch",
            port=9200,
        ),
        session,
    )
    es_hook = ElasticsearchHook("elasticsearch_test")
    es_hook.create_index(
        "raw",
        {
            "mappings": {
                "properties": {
                    "@timestamp": {"type": "date"},
                    "location": {
                        "type": "text",
                        "fields": {"keyword": {"type": "keyword", "ignore_above": 256}},
                    },
                    "qcode": {"type": "integer"},
                }
            }
        },
    )
    accessor = IntervalAccessor(
        es_hook,
        src_index="raw",
        dst_index="raw",
        location="TorrentDePareis",
        src_name="temperature",
        dst_name="temperature",
        reindex_frequency=timedelta(hours=1),
        process_interval=P.closedopen(
            iso_timestamp("2020-03-13T00:00:00"), iso_timestamp("2020-03-13T05:00:00")
        ),
    )
    yield accessor
    es_hook.delete_index("raw")


@pytest.fixture
def gaps_accessor():
    es_interval_storage_hook = ElasticsearchIntervalStorageHook("elasticsearch_test")

    class GapsAccessorMock(GapsAccessor):
        def get_all_gaps(self):
            gaps = P.IntervalDict()
            gaps[P.open(-P.inf, P.inf)] = True
            gaps[
                P.openclosed(
                    iso_timestamp("2020-03-13T00:00:00"),
                    iso_timestamp("2020-03-13T02:00:00"),
                )
            ] = False
            gaps[
                P.openclosed(
                    iso_timestamp("2020-03-13T04:00:00"),
                    iso_timestamp("2020-03-14T00:00:00"),
                )
            ] = False
            return gaps

    gaps_accessor = GapsAccessorMock(
        es_interval_storage_hook=es_interval_storage_hook,
        gaps_index=".intervals-config",
        location="TorrentDePareis",
        src_index="raw",
        src_name="temperature",
        process_interval=P.closedopen(
            iso_timestamp("2020-03-13T00:00:00"), iso_timestamp("2020-03-13T05:00:00")
        ),
    )
    yield gaps_accessor
