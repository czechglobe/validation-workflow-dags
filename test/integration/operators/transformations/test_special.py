import numpy as np

from test.utils import assert_dataframe_equals, generate_time_series
from validation_workflow.operators.interval_accessor import (
    IntervalAccessor,
    GapsAccessor,
)
from validation_workflow.operators.transformations.special import compute


def test_transformation_compute_simple(
    accessor: IntervalAccessor, gaps_accessor: GapsAccessor
):
    time_series = generate_time_series([1, 1, np.nan, np.nan, 4])
    accessor.set_time_series(time_series)

    accessor_a = accessor.subaccessor(src_name="a", dst_name="a")
    accessor_a.set_time_series(generate_time_series([6, 7, 8, 9, 10]))

    accessor_b = accessor.subaccessor(src_name="b", dst_name="b")
    accessor_b.set_time_series(generate_time_series([3, 3, 3, 3, 3]))

    compute(accessor, gaps_accessor, cmd="{a}+{b}")

    time_series = accessor.get_time_series()
    expected_time_series = generate_time_series([9, 10, 11, 12, 13])
    assert_dataframe_equals(time_series, expected_time_series)


def test_transformation_compute_param(
    accessor: IntervalAccessor, gaps_accessor: GapsAccessor
):
    time_series = generate_time_series([1, 1, np.nan, np.nan, 4])
    accessor.set_time_series(time_series)

    accessor_a = accessor.subaccessor(src_name="a", dst_name="a")
    accessor_a.set_time_series(generate_time_series([6, 7, 8, 9, 10]))

    compute(accessor, gaps_accessor, cmd="{a}+{b}", b=3)

    time_series = accessor.get_time_series()
    expected_time_series = generate_time_series([9, 10, 11, 12, 13])
    assert_dataframe_equals(time_series, expected_time_series)


def test_transformation_compute_qcode(
    accessor: IntervalAccessor, gaps_accessor: GapsAccessor
):
    time_series = generate_time_series([1, 1, np.nan, np.nan, 4])
    accessor.set_time_series(time_series)

    accessor_a = accessor.subaccessor(src_name="a", dst_name="a")
    accessor_a.set_time_series(generate_time_series([6, 7, 8, 9, 10], [1, 2, 1, 2, 1]))

    accessor_b = accessor.subaccessor(src_name="b", dst_name="b")
    accessor_b.set_time_series(generate_time_series([3, 3, 3, 3, 3], [1, 1, 3, 2, 1]))

    compute(accessor, gaps_accessor, cmd="{a}+{b}")

    time_series = accessor.get_time_series()
    expected_time_series = generate_time_series([9, 10, 11, 12, 13], [1, 2, 3, 2, 1])
    assert_dataframe_equals(time_series, expected_time_series)
