from datetime import timedelta

import pendulum
import portion as P
import pytest

from test.utils import generate_time_series, assert_dataframe_equals
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.operators.interval_accessor import IntervalAccessor
from validation_workflow.operators.simple_time_series_elastic_transform import (
    SimpleTimeSeriesElasticTransform,
)
from validation_workflow.utils import iso_timestamp


class TestTimeSeriesElasticTransform:
    @pytest.mark.usefixtures("es_interval_storage_hook")
    def test_time_series_elastic_transform(self, es_hook: ElasticsearchHook):
        accessor_raw = IntervalAccessor(
            es_hook,
            src_index="raw",
            dst_index="raw",
            location="TorrentDePareis",
            src_name="src-metric",
            dst_name="src-metric",
            reindex_frequency=timedelta(hours=1),
            process_interval=P.closedopen(
                iso_timestamp("2020-03-13"), iso_timestamp("2020-03-14")
            ),
        )
        accessor_processed = IntervalAccessor(
            es_hook,
            src_index="raw",
            dst_index="raw",
            location="TorrentDePareis",
            src_name="dst-metric",
            dst_name="dst-metric",
            reindex_frequency=timedelta(hours=1),
            process_interval=P.closedopen(
                iso_timestamp("2020-03-13"), iso_timestamp("2020-03-14")
            ),
        )

        accessor_raw.set_time_series(generate_time_series(list(range(24))))

        operator = SimpleTimeSeriesElasticTransform(
            task_id="test_task",
            location="TorrentDePareis",
            elasticsearch_conn_id=es_hook.elasticsearch_conn_id,
            src_index="raw",
            dst_index="raw",
            reindex_frequency=timedelta(hours=1),
            transforms=[
                ("src-metric", "dst-metric", "shift", {"value": 2.0, "qcode": 2})
            ],
        )

        context = {
            "execution_date": pendulum.datetime(2020, 3, 13, 0, 0, 0),
            "next_execution_date": pendulum.datetime(2020, 3, 14, 0, 0, 0),
        }

        operator.execute(context)

        time_series = accessor_processed.get_time_series()
        expected_time_series = generate_time_series(list(range(3, 24 + 3)), 2)
        assert_dataframe_equals(time_series, expected_time_series)
