import pendulum
import portion as P

from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.operators.user_validate_operator import UserValidateOperator
from validation_workflow.utils import iso_timestamp


class TestUserValidateOperator:
    def test_user_validate_operator(
        self,
        es_hook: ElasticsearchHook,
        es_interval_storage_hook: ElasticsearchIntervalStorageHook,
    ):
        operator = UserValidateOperator(
            task_id="test_task",
            location="TorrentDePareis",
            elasticsearch_conn_id=es_hook.elasticsearch_conn_id,
            config_step="raw",
            config_metrics=["temperature"],
            config_index=".intervals-config",
        )

        context = {
            "execution_date": pendulum.datetime(2020, 1, 16, 0, 0, 0),
            "next_execution_date": pendulum.datetime(2020, 1, 17, 0, 0, 0),
        }

        assert operator.check(**context) == False

        key = f"validated::{operator.location}::{operator.config_step}::{operator.config_metrics[0]}"

        intervals = P.IntervalDict()
        intervals[
            P.closedopen(iso_timestamp("2020-01-16"), iso_timestamp("2020-01-17"))
        ] = True
        es_interval_storage_hook.store_intervals(operator.config_index, key, intervals)

        assert operator.check(**context) == True

        intervals = P.IntervalDict()
        intervals[
            P.closedopen(
                iso_timestamp("2020-01-16"), iso_timestamp("2020-01-16T12:00:00")
            )
        ] = True
        intervals[
            P.closedopen(
                iso_timestamp("2020-01-01T12:00:00"), iso_timestamp("2020-01-17")
            )
        ] = False
        es_interval_storage_hook.store_intervals(operator.config_index, key, intervals)

        assert operator.check(**context) == False
