from datetime import timedelta

import portion as P

from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.operators.configured_time_series_elastic_transform import (
    ConfiguredTimeSeriesElasticTransform,
)
from validation_workflow.utils import iso_timestamp


class TestConfiguredTimeSeriesElasticTransform:
    def test_get_transforms(
        self,
        es_hook: ElasticsearchHook,
        es_interval_storage_hook: ElasticsearchIntervalStorageHook,
    ):
        operator = ConfiguredTimeSeriesElasticTransform(
            task_id="test_task",
            location="TorrentDePareis",
            elasticsearch_conn_id=es_hook.elasticsearch_conn_id,
            src_index="raw",
            dst_index="raw",
            reindex_frequency=timedelta(hours=1),
            config_step="raw",
            config_metrics=["temp10"],
            config_index=".intervals-config",
        )

        transforms = operator.get_transforms(P.open(-P.inf, P.inf))
        interval_expected = P.IntervalDict()
        interval_expected[P.open(-P.inf, P.inf)] = {
            "transformation": "nop",
            "transformation_params": {},
        }
        transforms_expected = [("temp10", "temp10", interval_expected)]
        assert transforms == transforms_expected

        key = f"transform::{operator.location}::{operator.config_step}::{operator.config_metrics[0]}"

        intervals_configured = P.IntervalDict()
        intervals_configured[
            P.open(iso_timestamp("2020-01-01"), iso_timestamp("2020-02-01"))
        ] = {"transformation": "scale", "transformation_params": {"coefficient": 2}}
        es_interval_storage_hook.store_intervals(
            operator.config_index, key, intervals_configured
        )

        transforms = operator.get_transforms(P.open(-P.inf, P.inf))
        interval_expected = P.IntervalDict()
        interval_expected[P.open(-P.inf, P.inf)] = {
            "transformation": "nop",
            "transformation_params": {},
        }
        interval_expected[
            P.open(iso_timestamp("2020-01-01"), iso_timestamp("2020-02-01"))
        ] = {"transformation": "scale", "transformation_params": {"coefficient": 2}}
        transforms_expected = [("temp10", "temp10", interval_expected)]
        assert transforms == transforms_expected
