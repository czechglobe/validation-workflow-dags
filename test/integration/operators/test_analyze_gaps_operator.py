from datetime import timedelta

import pandas as pd
import pendulum
import portion as P

from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.operators.analyze_gaps_operator import AnalyzeGapsOperator
from validation_workflow.operators.interval_accessor import IntervalAccessor
from validation_workflow.utils import iso_timestamp


class TestAnalyzeGapsOperator:
    def test_operator(
        self,
        es_hook: ElasticsearchHook,
        es_interval_storage_hook: ElasticsearchIntervalStorageHook,
    ):
        operator = AnalyzeGapsOperator(
            task_id="test_task",
            location="TorrentDePareis",
            elasticsearch_conn_id=es_hook.elasticsearch_conn_id,
            src_index="raw",
            gaps_metrics=["temperature"],
            gaps_index=".intervals-config",
            reindex_frequency=timedelta(hours=1),
        )

        context = {
            "execution_date": pendulum.datetime(2020, 1, 16, 0, 0, 0),
            "next_execution_date": pendulum.datetime(2020, 1, 17, 0, 0, 0),
        }

        present_interval = (
            P.closedopen(iso_timestamp("2020-01-01"), iso_timestamp("2020-02-01"))
            - P.closedopen(
                iso_timestamp("2020-01-12T12:00:00"),
                iso_timestamp("2020-01-16T04:00:00"),
            )
            - P.closedopen(
                iso_timestamp("2020-01-16T18:00:00"),
                iso_timestamp("2020-01-16T22:00:00"),
            )
        )

        accessor = IntervalAccessor(
            es_hook=es_hook,
            src_index="raw",
            dst_index="raw",
            location="TorrentDePareis",
            src_name="temperature",
            dst_name="temperature",
            reindex_frequency=timedelta(hours=1),
            process_interval=present_interval,
        )
        time_series = pd.DataFrame(
            {"value": 1, "qcode": 1},
            index=pd.date_range(iso_timestamp("2020-01-01"), periods=24 * 31, freq="H"),
        ).astype({"value": float, "qcode": int})
        accessor.set_time_series(time_series)

        operator.execute(context)

        gaps = es_interval_storage_hook.load_intervals(
            ".intervals-config",
            operator.metric_gap_key("temperature", context["execution_date"]),
        )

        expected = P.IntervalDict()
        expected[
            P.closedopen(iso_timestamp("2020-01-16"), iso_timestamp("2020-01-17"))
        ] = True
        expected[
            present_interval.intersection(
                P.closedopen(iso_timestamp("2020-01-16"), iso_timestamp("2020-01-17"))
            )
        ] = False

        assert gaps == expected
