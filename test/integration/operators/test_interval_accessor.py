from datetime import timedelta

import numpy as np
import pandas as pd
import portion as P

from test.utils import assert_dataframe_equals
from validation_workflow.operators.interval_accessor import IntervalAccessor
from validation_workflow.utils import iso_timestamp


class TestIntervalAccessor:
    def test_access(self, es_hook):
        day_interval = P.closedopen(
            iso_timestamp("2020-03-13"), iso_timestamp("2020-03-14")
        )

        accessor = IntervalAccessor(
            es_hook=es_hook,
            src_index="raw",
            dst_index="raw",
            location="TorrentDePareis",
            src_name="temperature",
            dst_name="temperature",
            reindex_frequency=timedelta(hours=1),
            process_interval=day_interval,
        )

        time_series = accessor.get_time_series()
        expected_time_series = pd.DataFrame(
            {"value": np.nan, "qcode": np.nan},
            index=pd.date_range(day_interval.lower, periods=24, freq="H"),
        ).astype({"value": object, "qcode": object})
        assert_dataframe_equals(time_series, expected_time_series)

        fill_time_series = pd.DataFrame(
            [{"value": i, "qcode": 1} for i in range(24)],
            index=pd.date_range(day_interval.lower, periods=24, freq="H"),
        ).astype({"value": object, "qcode": object})

        accessor.set_time_series(fill_time_series)

        time_series = accessor.get_time_series()
        assert_dataframe_equals(time_series, fill_time_series)
