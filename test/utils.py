import numpy as np
import pandas as pd

from validation_workflow.utils import iso_timestamp


def assert_dataframe_equals(time_series, expected_time_series):
    assert set(time_series.columns) == set(expected_time_series.columns)
    assert set(time_series.index) == set(expected_time_series.index)
    assert (time_series.isnull() == expected_time_series.isnull()).all().all()
    assert (time_series - expected_time_series).sum().sum() < 1e-6


def generate_time_series(values, qcodes=1, since="2020-03-13", freq="H"):
    if isinstance(qcodes, int):
        qcodes = [None if np.isnan(v) else qcodes for v in values]
    return pd.DataFrame(
        [{"value": value, "qcode": qcode} for value, qcode in zip(values, qcodes)],
        index=pd.date_range(iso_timestamp(since), periods=len(values), freq=freq),
    ).astype({"value": object, "qcode": object})
