from datetime import datetime, timedelta

import pandas as pd
from marshmallow import fields


class LooseDateTime(fields.DateTime):
    def _deserialize(self, value, attr, data):
        if isinstance(value, datetime):
            return value
        return pd.to_datetime(value).to_pydatetime()


class LooseTimeDelta(fields.TimeDelta):
    def _deserialize(self, value, attr, data):
        if isinstance(value, timedelta):
            return value
        return pd.to_timedelta(value).to_pytimedelta()
