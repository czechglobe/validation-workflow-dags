import os

default_config_path = os.path.join(os.path.dirname(__file__), "config.yaml")
config_path = os.getenv("VW_CONFIG_PATH", default_config_path)

conn_id_es = os.getenv("VW_CONN_ID_ELASTICSEARCH", "elasticsearch_db")

index_intervals_config = os.getenv("VW_INDEX_INTERVALS_CONFIG", ".intervals-config")
