import inspect
import itertools
from functools import lru_cache

import validation_workflow.operators.transformations.basic
import validation_workflow.operators.transformations.missing_values
import validation_workflow.operators.transformations.special


tr_modules = [
    validation_workflow.operators.transformations.basic,
    validation_workflow.operators.transformations.missing_values,
    validation_workflow.operators.transformations.special,
]

tr_funcs = {}
for tr_module in tr_modules:
    tr_funcs.update(
        {
            k: getattr(tr_module, k)
            for k in dir(tr_module)
            if hasattr(getattr(tr_module, k), "__fullname__")
        }
    )


@lru_cache()
def get_transform_list():
    return sorted(tr_funcs.keys())


def get_transform_func(transformation):
    if transformation not in tr_funcs:
        raise ValueError(f"Transformation {transformation} does not exist")
    return tr_funcs[transformation]


def inspect_function_params(func):
    argspec = inspect.getfullargspec(func)

    def type_function(param):
        if param in argspec.annotations:
            return argspec.annotations[param]
        return str

    params = reversed(
        [
            {"name": param, "default": default, "type": type_function(param)}
            for param, default in itertools.zip_longest(
                reversed(argspec.args), reversed(argspec.defaults or [])
            )
            if param not in ["accessor", "gaps_accessor", "_gaps_accessor"]
        ]
    )
    return list(params)


@lru_cache(maxsize=512)
def get_transform_params(transformation):
    tr_func = get_transform_func(transformation)
    function_params = inspect_function_params(tr_func)

    return {
        "id": tr_func.__name__,
        "name": tr_func.__fullname__,
        "category": tr_func.__category__,
        "description": tr_func.__doc__.strip(),
        "params": [
            {
                "name": param["name"],
                "default": param["default"],
                "type": param["type"].__name__,
            }
            for param in function_params
        ],
    }


def cast_transformation_params(transformation, transformation_params):
    res = transformation_params.copy()

    tr_func = get_transform_func(transformation)
    function_params = inspect_function_params(tr_func)

    for param in function_params:
        name = param["name"]
        default = param["default"]
        param_type = param["type"]

        if param["name"] in res:
            res[name] = param_type(res[name])
        else:
            if default:
                res[name] = param_type(default)
            else:
                raise ValueError(f"Missing transformation parameter `{name}`")

    return res
