import logging
from typing import Callable

import numpy as np
import pandas as pd
import portion as P

from validation_workflow.operators.interval_accessor import (
    IntervalAccessor,
    GapsAccessor,
)
from validation_workflow.utils import describe


def __process_gaps(
    accessor: IntervalAccessor,
    gaps_accessor: GapsAccessor,
    transform: Callable,
    transform_qcode: Callable,
    max_gap_time: str = None,
    min_present_time: str = None,
):
    if max_gap_time:
        max_gap_time = pd.to_timedelta(max_gap_time).to_pytimedelta()
    if min_present_time:
        min_present_time = pd.to_timedelta(min_present_time).to_pytimedelta()

    gaps = gaps_accessor.get_filtered_gaps(max_gap_time, min_present_time)
    time_series = accessor.get_time_series()
    for gap in gaps:
        if min_present_time:
            data_interval = P.closedopen(
                gap.lower - min_present_time, gap.upper + min_present_time
            )
        else:
            data_interval = gap
        gap_accessor = accessor.subaccessor(process_interval=data_interval)
        gap_time_series = gap_accessor.get_time_series()
        gap_time_series["value"] = transform(
            gap_time_series["value"].copy(), gap_accessor
        )
        gap_time_series["qcode"] = transform_qcode(gap_time_series["qcode"].copy())

        for i, row in gap_time_series.iterrows():
            time_series.loc[i] = row

    accessor.set_time_series(time_series)

    if len(gaps) == 0:
        logging.info("No gaps found.")


@describe(name="Linear interpolation", category="Missing values")
def linear_interpolation(
    accessor, gaps_accessor, qcode: int = 1, max_gap: str = "1 hour"
):
    """
    Linear interpolation of few missing values
    """

    def transform(gap_series, _):
        return gap_series.interpolate(method="linear")

    def transform_qcode(_):
        return qcode

    __process_gaps(
        accessor,
        gaps_accessor,
        transform,
        transform_qcode,
        max_gap_time=max_gap,
        min_present_time=accessor.reindex_frequency,
    )


@describe(name="Transplant", category="Missing values")
def transplant(
    accessor,
    gaps_accessor,
    qcode: int = 2,
    src_location: str = "<location>",
    src_name: str = "<metric name>",
    time_shift: str = "0 minutes",
    time_scale: float = 1.0,
    value_scale: float = 1.0,
):
    """
    Fill gaps with values transplanted from other metric
    """

    time_shift = pd.to_timedelta(time_shift).to_pytimedelta()

    def get_scaled_source_series(gap_accessor, gap_series_index):
        # gaps are atomic intervals so this part is simplified to support only continuous intervals
        interval_size = (
            gap_accessor.process_interval.upper - gap_accessor.process_interval.lower
        )
        scaled_interval = P.closedopen(
            gap_accessor.process_interval.lower + time_shift,
            gap_accessor.process_interval.lower
            + (interval_size * time_scale)
            + time_shift,
        )
        source_accessor = gap_accessor.subaccessor(
            process_interval=scaled_interval, src_name=src_name, location=src_location
        )
        scaled_series = source_accessor.get_time_series()
        scaled_series = pd.Series(
            scaled_series["value"].values,
            index=[
                gap_accessor.process_interval.lower
                + (t - scaled_interval.lower) * time_scale
                for t in scaled_series.index
            ],
        )
        scaled_series = scaled_series.resample(gap_accessor.reindex_frequency).mean()
        scaled_series = scaled_series.reindex(gap_series_index).interpolate(
            limit_direction="forward"
        )
        scaled_series = scaled_series * value_scale
        return scaled_series

    def transform(gap_series, gap_accessor):
        source_series = get_scaled_source_series(gap_accessor, gap_series.index)
        start_offset = gap_series[0] - source_series[0]
        end_offset = gap_series[-1] - source_series[-1]

        return source_series + np.linspace(start_offset, end_offset, len(gap_series))

    def transform_qcode(_):
        return qcode

    __process_gaps(
        accessor,
        gaps_accessor,
        transform,
        transform_qcode,
        min_present_time=accessor.reindex_frequency,
    )


@describe(name="Polynomial interpolation", category="Missing values")
def polynomial_interpolation(
    accessor, gaps_accessor, qcode: int = 2, order: int = 2, max_gap: str = "5 hours"
):
    """
    Linear interpolation of few missing values
    """

    def transform(gap_series, _):
        return gap_series.interpolate(method="polynomial", order=order)

    def transform_qcode(_):
        return qcode

    __process_gaps(
        accessor,
        gaps_accessor,
        transform,
        transform_qcode,
        max_gap_time=max_gap,
        min_present_time=order * accessor.reindex_frequency,
    )
