from typing import Callable

import numpy as np

from validation_workflow.operators.interval_accessor import IntervalAccessor
from validation_workflow.utils import describe


def __process_basic(
    accessor: IntervalAccessor, transform: Callable, transform_qcode: Callable
):
    time_series = accessor.get_time_series()
    time_series["value"] = transform(time_series["value"].copy())
    time_series["qcode"] = transform_qcode(time_series["qcode"].copy())
    accessor.set_time_series(time_series)


@describe(name="Identity", category="Basic")
def nop(accessor, _gaps_accessor):
    """
    Identity
    """

    def transform(series):
        return series

    def transform_qcode(qcode_series):
        return qcode_series

    __process_basic(accessor, transform, transform_qcode)


@describe(name="Scale", category="Basic")
def scale(accessor, _gaps_accessor, qcode: int = 1, value: float = 1.0):
    """
    Multiply metric with a static coefficient
    """

    def transform(series):
        return series * value

    def transform_qcode(_):
        return qcode

    __process_basic(accessor, transform, transform_qcode)


@describe(name="Shift", category="Basic")
def shift(accessor, _gaps_accessor, qcode: int = 1, value: float = 0.0):
    """
    Incrase or decrease value by given amount
    """

    def transform(series):
        return series + value

    def transform_qcode(_):
        return qcode

    __process_basic(accessor, transform, transform_qcode)


@describe(name="Constant", category="Basic")
def constant(accessor, _gaps_accessor, qcode: int = 2, value: float = 0.0):
    """
    Replace value with given constant
    """

    def transform(series):
        series[:] = value
        return series

    def transform_qcode(_):
        return qcode

    __process_basic(accessor, transform, transform_qcode)


@describe(name="Remove", category="Basic")
def remove(accessor, _gaps_accessor, qcode: int = 0):
    """
    Replace value with "missing"
    """

    def transform(series):
        series[:] = np.nan
        return series

    def transform_qcode(_):
        return qcode

    __process_basic(accessor, transform, transform_qcode)


@describe(name="Scale+Shift", category="Basic")
def scaleshift(
    accessor, _gaps_accessor, qcode: int = 1, a: float = 1.0, b: float = 0.0
):
    """
    Multiply metric with a static coefficient
    """

    def transform(series):
        return series * a + b

    def transform_qcode(_):
        return qcode

    __process_basic(accessor, transform, transform_qcode)
