import re

import pandas as pd
import portion as P

from validation_workflow.utils import describe

PARAM_PATTERN = re.compile(r"{(.*?)}")


@describe(name="Compute", category="Special")
def compute(accessor, _gaps_accessor, cmd: str, **transformation_params):
    """
    Perform any calculation
    """
    compute_inputs = PARAM_PATTERN.findall(cmd)

    time_series = accessor.get_time_series()

    # we assume all additional params are numebrs
    transformation_params = {k: float(v) for k, v in transformation_params.items()}

    transformation_qcodes = pd.DataFrame()

    for i, compute_input in enumerate(compute_inputs):
        # when this is existing param just use it
        if compute_input in transformation_params:
            cmd = cmd.replace("{" + compute_input + "}", compute_input)
            continue
        input_accessor = accessor.subaccessor(src_name=compute_input)
        input_time_series = input_accessor.get_time_series()
        # inputs may have nonstandard names so we have to replace them
        var = f"input_variable_{i}"
        transformation_params[var] = input_time_series["value"]
        transformation_qcodes[compute_input] = input_time_series["qcode"]

        cmd = cmd.replace("{" + compute_input + "}", var)

    # pylint: disable=eval-used
    transformed_series = eval(cmd, transformation_params)

    if not isinstance(transformed_series, pd.Series):
        raise ValueError("Result has to be pandas Series")

    time_series["value"] = transformed_series
    time_series["qcode"] = transformation_qcodes.max(axis=1)

    accessor.set_time_series(time_series)


@describe(name="Align to zero", category="Special")
def align_to_zero(
    accessor,
    _gaps_accessor,
    qcode: int = 0,
    window_size: str = "5 days",
    smoothing_size: str = "2 hours",
    clip_at_zero: bool = True,
):
    """
    Shift values so minimal value reads zero.
    """

    window_size = pd.to_timedelta(window_size).to_pytimedelta()
    smoothing_size = pd.to_timedelta(smoothing_size).to_pytimedelta()

    if window_size <= accessor.reindex_frequency:
        raise ValueError("window_size has to be larger than reindex_frequency")

    window_length = int(window_size / accessor.reindex_frequency)
    smoothing_length = int(smoothing_size / accessor.reindex_frequency)

    larger_interval = P.closedopen(
        accessor.process_interval.lower - window_size,
        accessor.process_interval.upper + window_size,
    )
    larger_accessor = accessor.subaccessor(process_interval=larger_interval)

    def transform(series):
        min_series = series.copy()
        if smoothing_length > 1:
            min_series = min_series.rolling(smoothing_length, center=True).mean()
        min_series = min_series.rolling(window_length, center=True).min()
        min_series = min_series.fillna(0)
        series = series - min_series
        if clip_at_zero:
            series = series.clip(lower=0)
        return series

    def transform_qcode(_):
        return qcode

    time_series = larger_accessor.get_time_series()
    time_series["value"] = transform(time_series["value"].copy())
    time_series["qcode"] = transform_qcode(time_series["qcode"].copy())
    accessor.set_time_series(time_series)
