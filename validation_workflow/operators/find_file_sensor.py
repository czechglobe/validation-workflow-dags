from typing import Union, List

from airflow.sensors.base_sensor_operator import BaseSensorOperator
from airflow.utils.decorators import apply_defaults

from validation_workflow.hooks.find_fs_hook import FindFSHook


class FindFileSensor(BaseSensorOperator):
    """
    Waits for a file or folder to land in a filesystem.

    If the path given is a directory then this sensor will only return true if
    any files exist inside it (either directly, or within a subdirectory)

    :param fs_conn_id: reference to the File (path)
        connection id
    :type fs_conn_id: string
    :param filepath: File or folder name (relative to
        the base path set within the connection)
    :type fs_conn_id: string
    """

    template_fields = ("filepath",)
    ui_color = "#91818a"

    @apply_defaults
    def __init__(
        self,
        filepath: Union[str, List[str]],
        fs_conn_id: str = "fs_default2",
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.filepath = filepath
        self.fs_conn_id = fs_conn_id

    def poke(self, context):
        hook = FindFSHook(self.fs_conn_id)
        self.log.info("Poking for file %s", self.filepath)
        files = hook.find_files(self.filepath)
        return len(files) > 0
