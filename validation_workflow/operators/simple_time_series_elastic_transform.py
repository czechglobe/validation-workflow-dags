from typing import List, Tuple

import portion as P

from airflow.utils.decorators import apply_defaults
from validation_workflow.operators.time_series_elastic_transform import (
    TimeSeriesElasticTransform,
)


class SimpleTimeSeriesElasticTransform(TimeSeriesElasticTransform):

    ui_color = "#efef3f"

    @apply_defaults
    def __init__(self, transforms: List[Tuple[str, str, str, dict]], *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.transforms = transforms

    def get_transforms(self, interval):
        def construct_endless_interval(transformation, transformation_params):
            settings = {
                "transformation": transformation,
                "transformation_params": transformation_params,
            }
            intervals = P.IntervalDict()
            intervals[interval] = settings
            return intervals

        return [
            (
                src_name,
                dst_name,
                construct_endless_interval(transformation, transformation_params),
            )
            for (
                src_name,
                dst_name,
                transformation,
                transformation_params,
            ) in self.transforms
        ]
