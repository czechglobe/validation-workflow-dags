from datetime import timedelta
from typing import List

import portion as P

from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.utils import iso_timestamp, iso_string, get_timestamps


class AnalyzeGapsOperator(BaseOperator):

    ui_color = "#e4edf0"

    @apply_defaults
    def __init__(
        self,
        location: str,
        elasticsearch_conn_id: str,
        src_index: str,
        reindex_frequency: timedelta,
        gaps_metrics: List[str],
        gaps_index: str = ".intervals-config",
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.location = location
        self.elasticsearch_conn_id = elasticsearch_conn_id
        self.src_index = src_index
        self.gaps_metrics = gaps_metrics
        self.gaps_index = gaps_index
        self.reindex_frequency = reindex_frequency

    def metric_gap_key(self, metric, execution_date):
        return f"gap::{self.location}::{self.src_index}::{metric}::{iso_string(execution_date)}"

    def execute(self, context):
        es_hook = ElasticsearchHook(self.elasticsearch_conn_id)
        es_interval_storage_hook = ElasticsearchIntervalStorageHook(
            self.elasticsearch_conn_id
        )

        executaion_interval = P.closedopen(
            iso_timestamp(context["execution_date"]),
            iso_timestamp(context["next_execution_date"]),
        )

        timestamps = get_timestamps(
            executaion_interval.lower, executaion_interval.upper, self.reindex_frequency
        )

        df = es_hook.load_fields_df(
            self.src_index, self.location, timestamps, self.gaps_metrics
        )
        df = df.isnull()

        all_intervals = {}
        for metric in self.gaps_metrics:
            key = self.metric_gap_key(metric, context["execution_date"])
            gaps = self.analyze_gaps(df, metric, executaion_interval)
            all_intervals[key] = gaps
        es_interval_storage_hook.store_multiple_intervals(
            self.gaps_index, all_intervals
        )

    def analyze_gaps(self, df, metric, executaion_interval):
        gaps = P.IntervalDict()
        gaps[executaion_interval] = False

        missing_timestamps = df.groupby(metric).groups.get(True, [])
        for timestamp in missing_timestamps:
            gaps[P.closedopen(timestamp, timestamp + self.reindex_frequency)] = True

        return gaps
