import abc
import logging
from datetime import timedelta

import portion as P

from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.operators.interval_accessor import (
    IntervalAccessor,
    GapsAccessor,
)
from validation_workflow.operators.transformations.utils import (
    get_transform_func,
    cast_transformation_params,
)
from validation_workflow.utils import iso_timestamp


class TimeSeriesElasticTransform(BaseOperator):

    ui_color = "#e4edf0"

    @apply_defaults
    def __init__(
        self,
        location: str,
        elasticsearch_conn_id: str,
        src_index: str,
        dst_index: str,
        reindex_frequency: timedelta = None,
        gaps_index: str = None,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.location = location
        self.elasticsearch_conn_id = elasticsearch_conn_id
        self.src_index = src_index
        self.dst_index = dst_index
        self.gaps_index = gaps_index
        self.reindex_frequency = reindex_frequency

    @abc.abstractmethod
    def get_transforms(self, interval):
        pass

    def execute(self, context):
        es_hook = ElasticsearchHook(self.elasticsearch_conn_id)
        es_interval_storage_hook = ElasticsearchIntervalStorageHook(
            self.elasticsearch_conn_id
        )

        logging.info("Loading transformations.")
        execution_interval = P.closedopen(
            iso_timestamp(context["execution_date"]),
            iso_timestamp(context["next_execution_date"]),
        )
        transforms = self.get_transforms(execution_interval)
        for src_name, dst_name, intervals in transforms:
            for process_interval, settings in intervals.items():
                self.process_interval(
                    es_hook,
                    es_interval_storage_hook,
                    src_name,
                    dst_name,
                    process_interval,
                    settings,
                )

    def process_interval(
        self,
        es_hook,
        es_interval_storage_hook,
        src_name,
        dst_name,
        process_interval,
        settings,
    ):
        transformation = settings["transformation"]
        transformation_params = settings["transformation_params"]

        logging.info(
            "Transforming metric %s using %s transformation (%s).",
            src_name,
            transformation,
            process_interval,
        )

        accessor = IntervalAccessor(
            es_hook,
            self.src_index,
            self.dst_index,
            self.location,
            src_name,
            dst_name,
            self.reindex_frequency,
            process_interval,
        )

        gaps_accessor = GapsAccessor(
            es_interval_storage_hook,
            self.gaps_index,
            self.location,
            self.src_index,
            src_name,
            process_interval,
        )

        tr_func = get_transform_func(transformation)
        transformation_params = cast_transformation_params(
            transformation, transformation_params
        )
        tr_func(accessor, gaps_accessor, **transformation_params)
