import logging
import re
from datetime import timedelta
from typing import List, Optional

import pandas as pd
import portion as P

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.operators.interval_accessor import IntervalAccessor
from validation_workflow.utils import iso_timestamp


class FinalTimeSeriesElasticTransform(BaseOperator):

    ui_color = "#04edf0"

    height_pattern = re.compile(r".*([\d]{1,3}%).*")

    @apply_defaults
    def __init__(
        self,
        location: str,
        elasticsearch_conn_id: str,
        src_index: str,
        dst_index: str,
        metrics: List[str],
        metrics_groups: Optional[List[Optional[str]]] = None,
        reindex_frequency: timedelta = None,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.location = location
        self.elasticsearch_conn_id = elasticsearch_conn_id
        self.src_index = src_index
        self.dst_index = dst_index
        self.reindex_frequency = reindex_frequency
        self.metrics = metrics

        if not metrics_groups:
            metrics_groups = [None for _ in metrics]

        self.metrics_groups = metrics_groups

    def execute(self, context):
        es_hook = ElasticsearchHook(self.elasticsearch_conn_id)

        execution_interval = P.closedopen(
            iso_timestamp(context["execution_date"]),
            iso_timestamp(context["next_execution_date"]),
        )

        for metric, metric_group in zip(self.metrics, self.metrics_groups):
            self.process_interval(es_hook, metric, metric_group, execution_interval)

    def process_interval(self, es_hook, metric, metric_group, execution_interval):
        logging.info("Transforming final metric %s", metric)

        accessor = IntervalAccessor(
            es_hook,
            self.src_index,
            self.dst_index,
            self.location,
            metric,
            metric,
            self.reindex_frequency,
            execution_interval,
        )

        time_series = accessor.get_time_series()
        self.enrich_time_series(time_series, metric, metric_group)
        accessor.set_time_series(time_series)

    def enrich_time_series(self, time_series, metric, metric_group):
        # height
        match = self.height_pattern.match(metric)
        if match:
            time_series["height"] = match.group(1)

        # hour_of_day
        time_series["hour_of_day"] = pd.Series(
            time_series.index, index=time_series.index
        ).dt.hour

        # month_of_year
        time_series["month_of_year"] = pd.Series(
            time_series.index, index=time_series.index
        ).dt.month

        if metric_group:
            time_series[metric_group] = time_series["value"]
