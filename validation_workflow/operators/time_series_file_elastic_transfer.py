import logging
import re
from datetime import timedelta
from typing import List

import numpy as np
import pandas as pd

from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.hooks.find_fs_hook import FindFSHook
from validation_workflow.utils import iso_timestamp, get_timestamps, fix_input_series


class TimeSeriesFileElasticTransfer(BaseOperator):

    template_fields = ("filepath",)
    ui_color = "#f0ede4"

    @apply_defaults
    def __init__(
        self,
        file_format: str,
        fs_conn_id: str,
        elasticsearch_conn_id: str,
        filepath: str,
        location: str,
        reindex_frequency: timedelta = None,
        columns: List[str] = None,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.file_format = file_format
        self.fs_conn_id = fs_conn_id
        self.elasticsearch_conn_id = elasticsearch_conn_id
        self.filepath = filepath
        self.reindex_frequency = reindex_frequency
        self.columns = columns
        self.location = location

        if self.file_format == "csv":
            self.df_callable = self.get_df_csv
            self.df_concat_axis = 0
        elif self.file_format == "tsv":
            self.df_callable = self.get_df_tsv
            self.df_concat_axis = 1
        elif self.file_format == "dat4":
            self.df_callable = self.get_df_dat4
            self.df_concat_axis = 0
        else:
            raise ValueError(f"File format {self.file_format} is not supported")

    def execute(self, context):
        fs_hook = FindFSHook(self.fs_conn_id)
        files = fs_hook.find_files(self.filepath)
        if len(files) == 0:
            logging.warning("No files matching filepath %s found", self.filepath)

        dfs = []
        for file in files:
            logging.info("Processing file %s", file)
            dfs.append(self.df_callable(file))

        if len(dfs) > 0:
            # some data can be splitted column wise and some row wise
            df = pd.concat(dfs, axis=self.df_concat_axis)
        else:
            df = pd.DataFrame()

        if self.reindex_frequency:
            timestamps = get_timestamps(
                iso_timestamp(context["execution_date"]),
                iso_timestamp(context["next_execution_date"]),
                self.reindex_frequency,
            )
            df = df.reindex(index=timestamps)

        if self.columns:
            for col in self.columns:
                if col not in df.columns:
                    logging.warning(
                        "Column %s not found. Present columns are only %s",
                        col,
                        ", ".join(df.columns.values),
                    )

            # remove duplicated columns, use first
            df = df.loc[:, ~df.columns.duplicated()]

            df = df.reindex(columns=self.columns)

        logging.info("Loaded DataFrame with shape %s,%s", df.shape[0], df.shape[1])

        es_hook = ElasticsearchHook(self.elasticsearch_conn_id)

        for col in df.columns:
            logging.info("Storing time series %s", col)
            time_series = pd.DataFrame(columns=["value", "qcode"])
            time_series["value"] = fix_input_series(df[col])
            time_series["qcode"] = 0
            es_hook.store_time_series("raw", self.location, col, time_series)

    @staticmethod
    def get_df_csv(file):
        df = pd.read_csv(
            file,
            parse_dates={"@timestamp": ["TIMESTAMP"]},
            date_parser=lambda col: pd.to_datetime(col, utc=True),
            index_col="@timestamp",
        )

        return df

    @staticmethod
    def get_df_tsv(file):
        header_sep = re.compile(r"#\d+ ")

        with open(file, "r") as f:
            header = f.readline()[:-1]
        columns = header_sep.split(header)
        columns[-1] = re.sub(r"#\d+", "", columns[-1])
        columns = ["DATE", "TIME"] + columns[1:]
        columns_index = [i for i in range(len(columns)) if len(columns[i]) > 0]
        columns = [c for c in columns if len(c) > 0]

        df = pd.read_csv(
            file,
            sep=" ",
            skiprows=1,
            usecols=columns_index,
            names=columns,
            parse_dates={"@timestamp": ["DATE", "TIME"]},
            date_parser=lambda col: pd.to_datetime(col, utc=True),
            index_col="@timestamp",
            dtype=float,
            na_values=["NAN"],
        )

        return df

    @staticmethod
    def get_df_dat4(file):
        df = pd.read_csv(
            file,
            sep=",",
            skiprows=[0, 2, 3],
            parse_dates={"@timestamp": ["TIMESTAMP"]},
            date_parser=lambda col: pd.to_datetime(col, utc=True),
            index_col="@timestamp",
            dtype=float,
            na_values=["NAN"],
        )

        return df
