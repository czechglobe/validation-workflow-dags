from datetime import timedelta

import pandas as pd
import portion as P

from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.utils import get_timestamps


class IntervalAccessor:
    def __init__(
        self,
        es_hook: ElasticsearchHook,
        src_index: str,
        dst_index: str,
        location: str,
        src_name: str,
        dst_name: str,
        reindex_frequency: timedelta,
        process_interval: P.Interval,
    ):
        self.es_hook = es_hook
        self.src_index = src_index
        self.dst_index = dst_index
        self.location = location
        self.src_name = src_name
        self.dst_name = dst_name
        self.reindex_frequency = reindex_frequency
        self.process_interval = process_interval

    def get_time_series(self):
        time_series = self.es_hook.load_time_series(
            self.src_index,
            self.location,
            self.src_name,
            self.process_interval.lower,
            self.process_interval.upper,
        )

        timestamps = get_timestamps(
            self.process_interval.lower,
            self.process_interval.upper,
            self.reindex_frequency,
        )
        time_series = time_series.reindex(index=timestamps)

        # interval can contain holes with so we have to check if timestamps are included in interval
        time_series = time_series.loc[
            pd.Series(time_series.index)
            .apply(lambda a: a in self.process_interval)
            .values
        ]

        return time_series

    def set_time_series(self, time_series):
        time_series = time_series.loc[
            pd.Series(time_series.index)
            .apply(lambda a: a in self.process_interval)
            .values
        ]

        self.es_hook.store_time_series(
            self.dst_index, self.location, self.dst_name, time_series
        )

    def subaccessor(self, **kwargs):
        subargs = {**self.__dict__, **kwargs}
        return IntervalAccessor(**subargs)


class GapsAccessor:
    def __init__(
        self,
        es_interval_storage_hook: ElasticsearchIntervalStorageHook,
        gaps_index: str,
        location: str,
        src_index: str,
        src_name: str,
        process_interval: P.Interval,
    ):
        self.es_interval_storage_hook = es_interval_storage_hook
        self.gaps_index = gaps_index
        self.location = location
        self.src_index = src_index
        self.src_name = src_name
        self.process_interval = process_interval

    def get_all_gaps(self):
        gaps_key = f"gap::{self.location}::{self.src_index}::{self.src_name}::"
        gaps = self.es_interval_storage_hook.load_merge_intervals_with_prefix(
            self.gaps_index, gaps_key, True
        )
        return gaps

    def get_filtered_gaps(self, max_gap_size=None, min_present_size=None):
        """
        Return: list of gap intervals which intersect process_interval
        """
        all_gaps = self.get_all_gaps()

        if min_present_size:
            gaps_dict = P.IntervalDict()
            gaps_dict[P.open(-P.inf, P.inf)] = True
            for interval in all_gaps.find(False):
                if not interval.empty:
                    if (interval.upper - interval.lower) >= min_present_size:
                        gaps_dict[interval] = False
        else:
            gaps_dict = all_gaps

        filtered_gaps = filter(
            lambda gap: not gap.intersection(self.process_interval).empty,
            gaps_dict.find(True),
        )

        filtered_gaps = filter(
            lambda gap: gap.lower != -P.inf and gap.upper != P.inf, filtered_gaps
        )

        if max_gap_size is not None:
            filtered_gaps = filter(
                lambda gap: (gap.upper - gap.lower) <= max_gap_size, filtered_gaps
            )

        return list(filtered_gaps)
