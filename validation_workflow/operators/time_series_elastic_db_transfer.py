import logging
from contextlib import closing
from typing import List, Tuple, Union

import pandas as pd

from airflow.hooks.base_hook import BaseHook
from airflow.hooks.dbapi_hook import DbApiHook

from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.utils import iso_timestamp, get_timestamps


def conflict_lines(cols):
    return ",\n".join(f"{column} = EXCLUDED.{column}" for column in cols)


def sanitize_columns(cols):
    return [f'"{col}"' for col in cols]


def upsert_values(
    db_hook: DbApiHook,
    table_name: str,
    columns: List[str],
    conflict_cols: Union[str, List[str]],
    update_cols: List[str],
    data: List[Tuple],
):
    """
    :param table_name
    :param columns: name of columns to insert
    :param conflict_cols: name of column(s) that should be used to determine conflict
    :param update_cols: name of columns to update if there is a conflict
    :param data: list of tuples, data to upsert
    """
    if isinstance(conflict_cols, str):
        conflict_cols = [conflict_cols]
    columns = sanitize_columns(columns)
    conflict_cols = sanitize_columns(conflict_cols)
    update_cols = sanitize_columns(update_cols)
    sql = f"""
        INSERT INTO {table_name}({', '.join(columns)})
        VALUES ({",".join(['%s'] * len(columns))})
        ON CONFLICT ({', '.join(conflict_cols)})
        DO UPDATE SET {conflict_lines(update_cols)}
    """
    with closing(db_hook.get_conn()) as conn:
        with closing(conn.cursor()) as cur:
            for row in data:
                cur.execute(sql, row)
        conn.commit()
    logging.info(f"{len(data)} rows upserted")


def ensure_table(db_hook: DbApiHook, dst_table, columns_types):
    columns_strings = [f'"{col}" {type}' for col, type in columns_types]
    sql = f"CREATE TABLE IF NOT EXISTS {dst_table} ({', '.join(columns_strings)})"
    with closing(db_hook.get_conn()) as conn:
        with closing(conn.cursor()) as cur:
            cur.execute(sql)
        conn.commit()


class TimeSeriesElasticDbTransfer(BaseOperator):

    ui_color = "#f0ede4"

    @apply_defaults
    def __init__(
        self,
        elasticsearch_conn_id: str,
        db_conn_id: str,
        location: str,
        src_index: str,
        dst_table: str,
        metrics: List[str],
        reindex_frequency,
        qcodes: bool = True,
        timestamp_column="timestamp",
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.elasticsearch_conn_id = elasticsearch_conn_id
        self.db_conn_id = db_conn_id
        self.location = location
        self.src_index = src_index
        self.dst_table = dst_table
        self.reindex_frequency = reindex_frequency
        self.metrics = metrics
        self.qcodes = qcodes
        self.timestamp_column = timestamp_column

    def execute(self, context):
        es_hook = ElasticsearchHook(self.elasticsearch_conn_id)
        db_hook = BaseHook.get_hook(self.db_conn_id)
        if not isinstance(db_hook, DbApiHook):
            raise ValueError(f"Connection {self.db_conn_id} is not a database")

        timestamps = get_timestamps(
            iso_timestamp(context["execution_date"]),
            iso_timestamp(context["execution_date"] + context["dag"].schedule_interval),
            self.reindex_frequency,
        )

        df = es_hook.load_fields_df(
            self.src_index, self.location, timestamps, self.metrics, qcodes=self.qcodes
        )

        columns = list(df.columns)
        columns_w_ts = [self.timestamp_column] + columns
        columns_types = [(self.timestamp_column, "TIMESTAMP PRIMARY KEY")] + [
            (column, "INTEGER" if column.endswith("_qcode") else "FLOAT")
            for column in columns
        ]

        df.index.name = self.timestamp_column
        df = df.reset_index()
        df[self.timestamp_column] = df[self.timestamp_column].dt.strftime(
            "%Y-%m-%dT%H:%M:%S"
        )
        df = df.where(pd.notnull(df), None)

        ensure_table(db_hook, self.dst_table, columns_types)

        data = df.to_records(index=False).tolist()
        upsert_values(
            db_hook, self.dst_table, columns_w_ts, self.timestamp_column, columns, data
        )
