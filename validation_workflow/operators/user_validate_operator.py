from typing import List

import portion as P

from airflow.operators.python_operator import ShortCircuitOperator
from airflow.utils.decorators import apply_defaults
from validation_workflow.models.validation_model import ValidationModel
from validation_workflow.utils import iso_timestamp


class UserValidateOperator(ShortCircuitOperator):

    ui_color = "#91818a"

    @apply_defaults
    def __init__(
        self,
        location: str,
        elasticsearch_conn_id: str,
        config_step: str,
        config_metrics: List[str],
        config_index: str = ".intervals-config",
        *args,
        **kwargs,
    ):
        super().__init__(python_callable=self.check, *args, **kwargs)
        self.location = location
        self.elasticsearch_conn_id = elasticsearch_conn_id
        self.config_index = config_index
        self.config_step = config_step
        self.config_metrics = config_metrics
        self.provide_context = True

    def check(self, *_, **context):

        executaion_interval = P.closedopen(
            iso_timestamp(context["execution_date"]),
            iso_timestamp(context["next_execution_date"]),
        )

        validation_model = ValidationModel(
            self.elasticsearch_conn_id, self.config_index
        )

        return validation_model.get_validated(
            self.location, self.config_step, self.config_metrics, executaion_interval
        )
