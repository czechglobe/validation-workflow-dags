from typing import List

from airflow.utils.decorators import apply_defaults
from validation_workflow.models.transform_model import TransformModel
from validation_workflow.operators.time_series_elastic_transform import (
    TimeSeriesElasticTransform,
)


class ConfiguredTimeSeriesElasticTransform(TimeSeriesElasticTransform):

    ui_color = "#efef3f"

    @apply_defaults
    def __init__(
        self,
        config_step: str,
        config_metrics: List[str],
        config_index: str = ".intervals-config",
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.config_index = config_index
        self.config_step = config_step
        self.config_metrics = config_metrics

    def get_transforms(self, interval):

        transform_model = TransformModel(self.elasticsearch_conn_id, self.config_index)
        all_intervals = transform_model.get_transforms(
            self.location, self.config_step, self.config_metrics, interval
        )

        return [
            (metric, metric, intervals) for metric, intervals in all_intervals.items()
        ]
