import os
from glob import iglob
from typing import Union, List

from airflow.contrib.hooks.fs_hook import FSHook


class FindFSHook(FSHook):
    """
    Allows for interaction with an file server.

    Connection should have a name and a path specified under extra:

    example:
    Conn Id: fs_test
    Conn Type: File (path)
    Host, Shchema, Login, Password, Port: empty
    Extra: {"path": "/tmp"}
    """

    def ifind_files(self, filepath):
        if not isinstance(filepath, list):
            filepath = [filepath]

        for path in filepath:
            full_path = os.path.join(self.basepath, path)
            for file in iglob(full_path):
                yield file

    def find_files(self, filepath: Union[str, List[str]]):
        return list(self.ifind_files(filepath))

    def get_conn(self):
        raise NotImplementedError()

    def get_records(self, sql):
        raise NotImplementedError()

    def get_pandas_df(self, sql):
        raise NotImplementedError()

    def run(self, sql):
        raise NotImplementedError()
