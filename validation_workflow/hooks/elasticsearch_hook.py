# pylint: disable=unexpected-keyword-arg
# for some reason pylint does not work well with es library and it raises unexpected-keyword-arg errors
import os
from datetime import datetime
from typing import List

import numpy as np
import pandas as pd
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from elasticsearch.helpers import scan

from airflow.hooks.base_hook import BaseHook
from validation_workflow.utils import iso_timestamp, iso_string


class ElasticsearchHook(BaseHook):
    conn_name_attr = "elasticsearch_conn_id"
    default_conn_name = "elasticsearch_default"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if len(args) == 1:
            self.elasticsearch_conn_id = args[0]
        elif self.conn_name_attr not in kwargs:
            self.elasticsearch_conn_id = self.default_conn_name
        else:
            self.elasticsearch_conn_id = kwargs[self.conn_name_attr]

        if self.elasticsearch_conn_id:
            conn = self.get_connection(self.elasticsearch_conn_id)
            params = {}
            if conn.login and conn.password:
                params["http_auth"] = (conn.login, conn.password)
            self.es = Elasticsearch(f"{conn.host}:{conn.port}", **params)

        self.refresh = "wait_for" if os.getenv("VW_SAFE_MODE", False) else None

    def get_conn(self):
        return self.es

    def get_records(self, sql):
        raise NotImplementedError()

    def get_pandas_df(self, sql):
        raise NotImplementedError()

    def run(self, sql):
        raise NotImplementedError()

    def create_index(self, index: str, body=None):
        self.es.indices.create(index=index, body=body, ignore=400)

    def delete_index(self, index: str):
        self.es.indices.delete(index=index)

    def insert(self, index: str, document_id, document: dict):
        self.es.index(index=index, id=document_id, body=document, refresh=self.refresh)

    def insert_all(self, index: str, documents: dict):
        def gendata():
            for document_id, document in documents.items():
                yield {"_index": index, "_id": document_id, "_source": document}

        bulk(self.es, gendata(), refresh=self.refresh)

    def search(self, index: str, body, size: int = 10):
        return self.es.search(index=index, body=body, size=size)

    def scan(self, index: str, body):
        return scan(self.es, index=index, query=body)

    def load_time_series(
        self, index: str, location: str, field_name: str, start_ts, end_ts
    ):
        current_scan = self.scan(
            index,
            {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"location": location}},
                            {
                                "range": {
                                    "@timestamp": {
                                        "gte": iso_timestamp(start_ts),
                                        "lt": iso_timestamp(end_ts),
                                    }
                                }
                            },
                            {"exists": {"field": field_name}},
                        ]
                    }
                },
                "size": 200,
            },
        )

        time_series_index = []
        time_series_data = []

        for document in current_scan:
            ts = pd.to_datetime(document["_source"]["@timestamp"], utc=True)
            value = document["_source"][field_name]
            qcode = document["_source"]["qcode"]

            time_series_index.append(ts)
            time_series_data.append((value, qcode))

        time_series = pd.DataFrame.from_records(
            time_series_data, index=time_series_index, columns=("value", "qcode")
        )

        return time_series

    def load_fields_df(
        self,
        index: str,
        location: str,
        timestamps: List[datetime],
        field_names: List[str],
        qcodes=False,
    ):
        start_ts = min(timestamps)
        end_ts = max(timestamps)

        source_common = ["@timestamp"]
        process_document = lambda document: document["_source"]
        column_names = field_names

        def process_document_qcode(document):
            source = document["_source"]
            for field in field_names:
                if field in source:
                    return {
                        "@timestamp": source["@timestamp"],
                        field: source[field],
                        f"{field}_qcode": source["qcode"],
                    }
            raise ValueError("Document should contain one of the fields")

        if qcodes:
            source_common += ["qcode"]
            process_document = process_document_qcode
            column_names = []
            for field in field_names:
                column_names.append(field)
                column_names.append(f"{field}_qcode")

        current_scan = self.scan(
            index,
            {
                "query": {
                    "bool": {
                        "must": [
                            {"match": {"location": location}},
                            {
                                "range": {
                                    "@timestamp": {
                                        "gte": iso_timestamp(start_ts),
                                        "lte": iso_timestamp(end_ts),
                                    }
                                }
                            },
                        ]
                    }
                },
                "_source": source_common + field_names,
            },
        )

        docs = [process_document(document) for document in current_scan]
        if docs:
            df = pd.DataFrame(docs)
            df["@timestamp"] = pd.to_datetime(df["@timestamp"], utc=True)
            df = df.set_index("@timestamp").groupby(level=0).max()
            df = df.reindex(index=timestamps, columns=column_names)
        else:
            df = pd.DataFrame(index=timestamps, columns=column_names)

        return df

    def store_time_series(self, index, location, field_name, time_series):
        if not isinstance(time_series, pd.DataFrame):
            raise ValueError("Parameter time_series has to be a DataFrame")
        required_columns = {"value", "qcode"}
        for required_column in required_columns:
            if required_column not in time_series.columns:
                raise ValueError(
                    f"Parameter time_series is missing a columns {required_column}"
                )

        time_series.index.name = "@timestamp"
        df = time_series.reset_index()
        df.index = df["@timestamp"].apply(
            lambda timestamp: f"{location}_{field_name}_{timestamp}"
        )
        df = df.rename(columns={"value": field_name})
        df["@timestamp"] = df["@timestamp"].dt.to_pydatetime()
        df["location"] = location
        df = df.where(pd.notnull(df), None)

        documents = df.to_dict(orient="index")

        self.insert_all(index, documents)
