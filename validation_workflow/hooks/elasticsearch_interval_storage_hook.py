# pylint: disable=unexpected-keyword-arg
# for some reason pylint does not work well with es library and it raises unexpected-keyword-arg errors

from typing import List, Dict

import pandas as pd
import portion as P

from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook


class ElasticsearchIntervalStorageHook(ElasticsearchHook):
    def get_conn(self):
        return self.es

    def get_records(self, sql):
        raise NotImplementedError()

    def get_pandas_df(self, sql):
        raise NotImplementedError()

    def run(self, sql):
        raise NotImplementedError()

    def load_multiple_intervals(self, index: str, keys: List[str], default=None):
        scan = self.scan(index, {"query": {"terms": {"key": keys}}, "size": 200})

        all_intervals = {}

        for document in scan:
            key = document["_source"]["key"]
            intervals = P.IntervalDict()
            if default is not None:
                intervals[P.open(-P.inf, P.inf)] = default
            for fragment in document["_source"]["fragments"]:
                interval = P.from_string(
                    fragment["interval"], conv=lambda s: pd.to_datetime(s, utc=True)
                )
                settings = fragment["settings"]
                intervals[interval] = settings
            all_intervals[key] = intervals

        for key in keys:
            if key not in all_intervals:
                all_intervals[key] = P.IntervalDict()
                if default is not None:
                    all_intervals[key][P.open(-P.inf, P.inf)] = default

        return all_intervals

    def load_merge_intervals_with_prefix(self, index: str, prefix: str, default=None):
        scan = self.scan(index, {"query": {"prefix": {"key": prefix}}, "size": 200})

        merged_intervals = P.IntervalDict()
        if default is not None:
            merged_intervals[P.open(-P.inf, P.inf)] = default

        for document in scan:
            for fragment in document["_source"]["fragments"]:
                interval = P.from_string(
                    fragment["interval"], conv=lambda s: pd.to_datetime(s, utc=True)
                )
                settings = fragment["settings"]
                merged_intervals[interval] = settings

        return merged_intervals

    def load_intervals(self, index: str, key: str, default=None):
        return self.load_multiple_intervals(index, [key], default)[key]

    def _intervals_to_fragments(self, intervals):
        return [
            {
                "interval": P.to_string(interval, conv=lambda ts: ts.isoformat()),
                "settings": settings,
            }
            for interval, settings in intervals.items()
        ]

    def store_multiple_intervals(
        self, index: str, all_intervals: Dict[str, P.IntervalDict]
    ):
        documents = {
            key: {"fragments": self._intervals_to_fragments(intervals), "key": key}
            for key, intervals in all_intervals.items()
        }
        self.insert_all(index, documents)

    def store_intervals(self, index: str, key: str, intervals: P.IntervalDict):
        self.insert(
            index,
            key,
            {"fragments": self._intervals_to_fragments(intervals), "key": key},
        )
