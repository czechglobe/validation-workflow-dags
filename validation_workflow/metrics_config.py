from dataclasses import dataclass, field
from datetime import datetime, timedelta
from typing import List, Dict, Optional, Iterable, Any

import yaml
from marshmallow import Schema, fields, post_load, ValidationError, pre_load, validates
from airflow.models import Variable
from airflow.utils.db import provide_session

from validation_workflow import settings, vw_fields
from validation_workflow.utils import idealize, ALL_STEPS, get_duplicated

VARIABLES_PREFIX = "metrics_config_"


@dataclass
class MetricConfig:
    name_raw: Optional[str] = None
    name_unit: Optional[str] = None
    name_final: Optional[str] = None
    transformation: str = "nop"
    transformation_params: Dict = field(default_factory=dict)
    final_group: Optional[str] = None


@dataclass
class LocationConfigDagArgs:
    start_date: datetime
    end_date: Optional[datetime] = None
    retry_delay: Optional[timedelta] = timedelta(minutes=5)
    retries: Optional[int] = 3
    schedule_interval: Optional[timedelta] = timedelta(days=1)


@dataclass
class LocationExport:
    conn_id: str
    table: str = None


@dataclass
class LocationConfig:
    name: str
    dag_args: LocationConfigDagArgs
    metrics: List[MetricConfig]
    frequency: timedelta = timedelta(minutes=10)
    source_format: str = None
    source: List[str] = None
    export: LocationExport = None


class MetricConfigSchema(Schema):
    name_raw = fields.Str()
    name_unit = fields.Str()
    name_final = fields.Str()
    transformation = fields.Str()
    transformation_params = fields.Dict()
    final_group = fields.Str()

    @pre_load
    def pre_load(self, data, **_kwargs):
        if "name" in data:
            data["name_raw"] = data["name_unit"] = data["name_final"] = data["name"]
        return data

    @post_load
    def post_load(self, data, **_kwargs):
        if "transformation" in data and data["transformation"].startswith("="):
            compute = data["transformation"][1:]
            data["transformation"] = "compute"
            if "transformation_params" not in data:
                data["transformation_params"] = {}
            data["transformation_params"]["cmd"] = compute
        return MetricConfig(**data)


class LocationConfigDagArgsSchema(Schema):
    start_date = vw_fields.LooseDateTime(required=True)
    end_date = vw_fields.LooseDateTime()
    retry_delay = vw_fields.LooseTimeDelta()
    retries = fields.Int()
    schedule_interval = vw_fields.LooseTimeDelta()

    @post_load
    def post_load(self, data, **_kwargs):
        return LocationConfigDagArgs(**data)


class LocationExportSchema(Schema):
    conn_id = fields.Str(required=True)
    table = fields.Str(required=False)

    @post_load
    def post_load(self, data, **_kwargs):
        return LocationExport(**data)


class LocationConfigSchema(Schema):
    name = fields.Str(required=True)
    source_format = fields.Str()
    source = fields.List(fields.Str())
    dag_args = fields.Nested(LocationConfigDagArgsSchema, required=True)
    metrics = fields.List(fields.Nested(MetricConfigSchema), required=True)
    frequency = vw_fields.LooseTimeDelta()
    export = fields.Nested(LocationExportSchema)

    @pre_load
    def pre_load(self, data, **_kwargs):
        if "source" in data and isinstance(data["source"], str):
            data["source"] = [data["source"]]
        return data

    @validates("metrics")
    def validate_quantity(self, metrics):
        for step in ALL_STEPS:
            names = [
                getattr(m, f"name_{step}")
                for m in metrics
                if getattr(m, f"name_{step}")
            ]
            duplicated_names = get_duplicated(names)
            if duplicated_names:
                raise ValidationError(
                    f"There is a duplicate names of metrics in step {step}: {', '.join(duplicated_names)}"
                )

    @post_load
    def post_load(self, data, **_kwargs):
        return LocationConfig(**data)


class MetricsConfig:
    def __init__(self):
        self.locations = {}

    def load_from_file(self, path: str):
        with open(path) as file:
            self.load(file.read())

    def load(self, content: str):
        config = yaml.load(content, Loader=yaml.FullLoader)
        self.load_locations_config(config["locations"])

    def load_locations_config(self, locations: Dict):
        for location_name, config in locations.items():
            location_id = idealize(location_name)
            config["name"] = location_name

            config_result = LocationConfigSchema().load(config)
            if config_result.errors:
                raise ValidationError(config_result.errors, location=location_name)

            self.locations[location_id] = config_result.data

    def get_locations(self) -> Iterable[str]:
        return self.locations.keys()

    def get_location_config(self, location_id: str) -> LocationConfig:
        return self.locations[location_id]


@provide_session
def load_metrics_config(session=None) -> MetricsConfig:
    metrics_config = MetricsConfig()

    try:
        metrics_config.load_from_file(settings.config_path)
    except Exception as e:
        raise ValueError(
            f"Cannot parse config file {settings.config_path}: {str(e)}"
        ) from e

    variables = (
        session.query(Variable).filter(Variable.key.like(VARIABLES_PREFIX + "%")).all()
    )
    for variable in variables:
        try:
            metrics_config.load(variable.val)
        except Exception as e:
            raise ValueError(f"Cannot parse variable {variable.key}: {str(e)}") from e

    return metrics_config
