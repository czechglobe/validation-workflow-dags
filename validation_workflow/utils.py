import re
import unicodedata
from datetime import datetime, timedelta
from typing import Any, Iterable, List, Set

import pandas as pd
import numpy as np

ALL_STEPS = ["raw", "unit", "final"]
EDITABLE_STEPS = ["raw", "unit"]

UNDEFINED_VALUE = -999.5


def idealize(string):
    a = "".join(
        c
        for c in unicodedata.normalize("NFD", string)
        if unicodedata.category(c) != "Mn"
    ).lower()
    b = re.sub(r"[^a-z0-9\._]", "-", a)
    c = re.sub(r"[\.]", "_", b)
    return c


def iso_timestamp(val) -> datetime:
    if isinstance(val, str):
        return pd.to_datetime(
            pd.to_datetime(val).strftime("%Y-%m-%dT%H:%M:%S")
        ).tz_localize("UTC")
    if hasattr(val, "strftime"):
        return pd.to_datetime(val.strftime("%Y-%m-%dT%H:%M:%S")).tz_localize("UTC")
    raise NotImplementedError()


def iso_string(val: datetime) -> str:
    if hasattr(val, "strftime"):
        return val.strftime("%Y-%m-%dT%H:%M:%S")
    raise NotImplementedError()


def get_timestamps(
    interval_from: datetime, interval_to: datetime, reindex_frequency: timedelta
):
    """
    Get timestamps with given frequency aligned to day
    """
    timestamps = pd.date_range(
        interval_from.floor("D"), interval_to.ceil("D"), freq=reindex_frequency
    )
    return timestamps[(timestamps >= interval_from) & (timestamps < interval_to)]


def get_duplicated(values: Iterable[Any]) -> Set[Any]:
    seen = set()
    duplicated = set()
    for x in values:
        if x not in seen:
            seen.add(x)
        else:
            duplicated.add(x)
    return duplicated


# function decorator describe
def describe(name: str, category: str):
    def ret_fun(tr_func):
        tr_func.__fullname__ = name
        tr_func.__category__ = category
        return tr_func

    return ret_fun


def fix_input_series(series):
    series = pd.to_numeric(series, errors="coerce").astype(float)

    # Special value -999 is used as undefined value in some data sources so we have to
    # remove it. This value should not appera in correct readings.
    series = series.mask(np.isclose(series.values, UNDEFINED_VALUE, 0.51))

    return series
