# pylint: disable=no-value-for-parameter,pointless-statement
from airflow import DAG
from validation_workflow.operators.user_validate_operator import UserValidateOperator
from validation_workflow import settings
from validation_workflow.metrics_config import load_metrics_config
from validation_workflow.operators.analyze_gaps_operator import AnalyzeGapsOperator
from validation_workflow.operators.configured_time_series_elastic_transform import (
    ConfiguredTimeSeriesElasticTransform,
)
from validation_workflow.operators.final_time_series_elastic_transform import (
    FinalTimeSeriesElasticTransform,
)
from validation_workflow.operators.find_file_sensor import FindFileSensor
from validation_workflow.operators.simple_time_series_elastic_transform import (
    SimpleTimeSeriesElasticTransform,
)
from validation_workflow.operators.time_series_file_elastic_transfer import (
    TimeSeriesFileElasticTransfer,
)
from validation_workflow.utils import idealize

metrics_config = load_metrics_config()


for location_id in metrics_config.get_locations():
    location_config = metrics_config.get_location_config(location_id)
    location_name = location_config.name

    dag = DAG(
        dag_id=location_id,
        default_args={
            "owner": "CzechGlobe",
            "depends_on_past": False,
            "start_date": location_config.dag_args.start_date,
            "end_date": location_config.dag_args.end_date,
            "retries": location_config.dag_args.retries,
            "retry_delay": location_config.dag_args.retry_delay,
            "params": {"location": location_id, "location_name": location_name},
            # passed into all operators
            "location": location_id,
            "elasticsearch_conn_id": settings.conn_id_es,
            "config_index": settings.index_intervals_config,
            "gaps_index": settings.index_intervals_config,
        },
        schedule_interval=location_config.dag_args.schedule_interval,
        max_active_runs=8,
        concurrency=84,
    )
    globals()[dag.dag_id] = dag

    raw_data_sensor = download_raw_data = None
    if location_config.source and location_config.source_format:

        raw_data_sensor = FindFileSensor(
            task_id="raw_data_sensor",
            dag=dag,
            fs_conn_id="input_fs",
            filepath=location_config.source,
            poke_interval=60 * 60 * 24,
            timeout=60 * 60 * 24 * 356,
            mode="reschedule",
        )

        download_raw_data = TimeSeriesFileElasticTransfer(
            task_id="download_raw_data",
            dag=dag,
            file_format=location_config.source_format,
            fs_conn_id="input_fs",
            filepath=location_config.source,
            reindex_frequency=None,  # do not force uniform steps at this stage
            columns=[
                metric.name_raw for metric in location_config.metrics if metric.name_raw
            ],
        )

    analyze_raw_data_gaps = AnalyzeGapsOperator(
        task_id="analyze_raw_data_gaps",
        dag=dag,
        src_index="raw",
        reindex_frequency=location_config.frequency,
        gaps_metrics=[
            metric.name_raw for metric in location_config.metrics if metric.name_raw
        ],
    )

    fix_raw_data_tasks = {}
    for metric in location_config.metrics:
        if metric.name_raw:
            fix_raw_data_tasks[metric.name_raw] = ConfiguredTimeSeriesElasticTransform(
                task_id=f"fix_raw_data_{idealize(metric.name_raw)}",
                dag=dag,
                src_index="raw",
                dst_index="raw_fixed",
                reindex_frequency=location_config.frequency,
                config_step="raw",
                config_metrics=[metric.name_raw],
            )

    transform_to_physical_units = SimpleTimeSeriesElasticTransform(
        task_id="transform_to_physical_units",
        dag=dag,
        src_index="raw_fixed",
        dst_index="unit",
        reindex_frequency=location_config.frequency,
        transforms=[
            (
                metric.name_raw,
                metric.name_unit,
                metric.transformation,
                metric.transformation_params,
            )
            for metric in location_config.metrics
            if metric.name_raw and metric.name_unit
        ],
    )

    analyze_unit_data_gaps = AnalyzeGapsOperator(
        task_id="analyze_unit_data_gaps",
        dag=dag,
        src_index="unit",
        reindex_frequency=location_config.frequency,
        gaps_metrics=[
            metric.name_unit for metric in location_config.metrics if metric.name_unit
        ],
    )

    fix_unit_data_tasks = {}
    for metric in location_config.metrics:
        if metric.name_unit:
            fix_unit_data_tasks[
                metric.name_unit
            ] = ConfiguredTimeSeriesElasticTransform(
                task_id=f"fix_unit_data_{idealize(metric.name_unit)}",
                dag=dag,
                src_index="unit",
                dst_index="unit_fixed",
                reindex_frequency=location_config.frequency,
                config_step="unit",
                config_metrics=[metric.name_unit],
            )

    validate = UserValidateOperator(
        task_id="validate",
        dag=dag,
        config_step="unit",
        config_metrics=[
            metric.name_final for metric in location_config.metrics if metric.name_final
        ],
    )

    store_final_data = FinalTimeSeriesElasticTransform(
        task_id="store_final_data",
        dag=dag,
        src_index="unit_fixed",
        dst_index="final",
        reindex_frequency=location_config.frequency,
        metrics=[
            metric.name_final for metric in location_config.metrics if metric.name_final
        ],
        metrics_groups=[
            metric.final_group
            for metric in location_config.metrics
            if metric.name_final
        ],
    )

    if raw_data_sensor and download_raw_data:
        raw_data_sensor >> download_raw_data >> analyze_raw_data_gaps

    for t in fix_raw_data_tasks.values():
        analyze_raw_data_gaps >> t
        t >> transform_to_physical_units

    transform_to_physical_units >> analyze_unit_data_gaps

    for t in fix_unit_data_tasks.values():
        analyze_unit_data_gaps >> t
        t >> store_final_data

    validate >> store_final_data
