# pylint: disable=no-value-for-parameter,pointless-statement
from datetime import timedelta

from airflow import DAG
from validation_workflow.operators.time_series_elastic_db_transfer import (
    TimeSeriesElasticDbTransfer,
)
from validation_workflow.operators.user_validate_operator import UserValidateOperator
from validation_workflow import settings
from validation_workflow.metrics_config import load_metrics_config

metrics_config = load_metrics_config()

dag = DAG(
    dag_id="db_export",
    default_args={
        "owner": "CzechGlobe",
        "depends_on_past": False,
        "retries": 5,
        "retry_delay": timedelta(minutes=10),
    },
    schedule_interval=timedelta(weeks=1),
    max_active_runs=1,
)

for location_id in metrics_config.get_locations():
    location_config = metrics_config.get_location_config(location_id)

    if location_config.export:
        validate = UserValidateOperator(
            task_id=f"validate_{location_id}",
            dag=dag,
            start_date=location_config.dag_args.start_date,
            end_date=location_config.dag_args.end_date,
            elasticsearch_conn_id=settings.conn_id_es,
            location=location_id,
            config_step="unit",
            config_metrics=[
                metric.name_final
                for metric in location_config.metrics
                if metric.name_final
            ],
        )

        db_export = TimeSeriesElasticDbTransfer(
            task_id=f"export_{location_id}",
            dag=dag,
            start_date=location_config.dag_args.start_date,
            end_date=location_config.dag_args.end_date,
            elasticsearch_conn_id=settings.conn_id_es,
            db_conn_id=location_config.export.conn_id,
            location=location_id,
            src_index="final",
            dst_table=location_config.export.table or location_id,
            metrics=[
                metric.name_final
                for metric in location_config.metrics
                if metric.name_final
            ],
            reindex_frequency=location_config.frequency,
            qcodes=True,
        )

        validate >> db_export
