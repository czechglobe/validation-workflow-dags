import os
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
from perlin_noise import PerlinNoise

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from validation_workflow.hooks.find_fs_hook import FindFSHook
from validation_workflow.metrics_config import LocationConfig, load_metrics_config

metrics_config = load_metrics_config()

dag = DAG(
    dag_id="demo_data_generator",
    default_args={
        "owner": "CzechGlobe",
        "depends_on_past": False,
        "start_date": datetime(2020, 1, 1),
        "retries": 2,
    },
    schedule_interval=timedelta(days=1),
    max_active_runs=8,
)


def generate_file_callable(params, **context):
    noise = PerlinNoise(
        octaves=params["values"]["octaves"], seed=params["values"]["seed"]
    )
    missing_noise = PerlinNoise(
        octaves=params["missing_large"]["octaves"], seed=params["missing_large"]["seed"]
    )

    def metric_to_num(metric, scale=1.0):
        return sum((ord(c) - 48) * scale for c in metric)

    index = pd.date_range(
        context["ds"], context["next_ds"], freq="1h", closed="left", name="TIMESTAMP"
    )
    location_num = metric_to_num(params["location"])
    data = []
    for time in index:
        t = (time.timestamp() / 3600.0) * params["values"]["scale"]
        data.append(
            {
                metric: noise([t, location_num + metric_to_num(metric, scale=0.0194)])
                for metric in params["metrics"]
            }
        )

        t = (time.timestamp() / 3600.0) * params["missing_large"]["scale"]
        for metric in params["metrics"]:
            missing = (
                missing_noise([t, location_num + metric_to_num(metric, scale=0.2)])
                > params["missing_large"]["threashold"]
            )
            if missing:
                data[-1][metric] = np.nan

        t = (time.timestamp() / 3600.0) * params["missing_tiny"]["scale"]
        for metric in params["metrics"]:
            missing = (
                missing_noise([t, location_num + metric_to_num(metric, scale=0.4)])
                > params["missing_tiny"]["threashold"]
            )
            if missing:
                data[-1][metric] = np.nan

    df = pd.DataFrame(data, index=index)

    fs_hook = FindFSHook(params["fs_conn_id"])
    path = os.path.join(fs_hook.basepath, context["templates_dict"]["file_path"])

    if not os.path.isdir(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    df.to_csv(path)


for location_id in metrics_config.get_locations():
    location_config: LocationConfig = metrics_config.get_location_config(location_id)

    if not location_id.startswith("demo_"):
        continue

    PythonOperator(
        task_id=f"generate_file_{location_id}",
        dag=dag,
        python_callable=generate_file_callable,
        provide_context=True,
        params={
            "fs_conn_id": "input_fs",
            "location": location_id,
            "metrics": [
                metric.name_raw for metric in location_config.metrics if metric.name_raw
            ],
            "values": {"seed": 1, "octaves": 6, "scale": 0.0271828},
            "missing_large": {
                "seed": 84,
                "octaves": 11,
                "scale": 0.00005,
                "threashold": 0.2,
            },
            "missing_tiny": {
                "seed": 49,
                "octaves": 27,
                "scale": 0.025,
                "threashold": 0.4,
            },
        },
        templates_dict={"file_path": location_config.source[0]},
    )
