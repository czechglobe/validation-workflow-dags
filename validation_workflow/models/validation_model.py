from typing import List

import portion as P

from validation_workflow.metrics_config import MetricConfig
from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)
from validation_workflow.utils import ALL_STEPS


class ValidationModel:
    def __init__(self, elasticsearch_conn_id, config_index):
        self.hook = ElasticsearchIntervalStorageHook(elasticsearch_conn_id)
        self.config_index = config_index

    def get_validated(
        self, location: str, step: str, metrics: List[str], interval: P.Interval
    ):
        keys = [self.get_key(location, step, metric) for metric in metrics]
        all_intervals = self.hook.load_multiple_intervals(
            self.config_index, keys, self.get_default()
        )

        for key, intervals in all_intervals.items():
            if not intervals.find(True).contains(interval):
                return False
        return True

    def set_validated(
        self,
        location: str,
        step: str,
        metrics: List[str],
        interval: P.Interval,
        validated: bool,
    ):
        keys = [self.get_key(location, step, metric) for metric in metrics]
        all_intervals = self.hook.load_multiple_intervals(
            self.config_index, keys, self.get_default()
        )

        for metric in metrics:
            all_intervals[self.get_key(location, step, metric)][interval] = validated

        self.hook.store_multiple_intervals(self.config_index, all_intervals)

    def get_metric_status(
        self, location: str, interval: P.Interval, metrics: List[MetricConfig]
    ):
        result = {step: P.IntervalDict() for step in ALL_STEPS}

        keys = [
            f"validated::{location}::{step}::{getattr(m, f'name_{step}')}"
            for step in ALL_STEPS
            for m in metrics
            if getattr(m, f"name_{step}")
        ]
        all_intervals = self.hook.load_multiple_intervals(self.config_index, keys, None)

        # pessimistic reduction to one interval for each step (nonvalidated > validated)
        for step in ALL_STEPS:
            result[step][interval] = "nonvalidated"

            for m in metrics:
                if getattr(m, f"name_{step}"):
                    metric_name = getattr(m, f"name_{step}")
                    key = f"validated::{location}::{step}::{metric_name}"
                    validated_interval = all_intervals[key][interval].find(True)
                    result[step][validated_interval] = "validated"

            for m in metrics:
                if getattr(m, f"name_{step}"):
                    metric_name = getattr(m, f"name_{step}")
                    key = f"validated::{location}::{step}::{metric_name}"
                    nonvalidated_interval = all_intervals[key][interval].find(False)
                    result[step][nonvalidated_interval] = "nonvalidated"

        return result

    def get_key(self, location, step, metric):
        return f"validated::{location}::{step}::{metric}"

    def get_default(self):
        return False
