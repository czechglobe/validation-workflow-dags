from typing import List

import portion as P

from validation_workflow.hooks.elasticsearch_interval_storage_hook import (
    ElasticsearchIntervalStorageHook,
)


class TransformModel:
    def __init__(self, elasticsearch_conn_id, config_index):
        self.hook = ElasticsearchIntervalStorageHook(elasticsearch_conn_id)
        self.config_index = config_index

    def get_transform(
        self, location: str, step: str, metric: str, interval: P.Interval
    ):
        key = self.get_key(location, step, metric)
        intervals = self.hook.load_intervals(self.config_index, key, self.get_default())

        return intervals[interval]

    def set_transform(
        self, location: str, step: str, metric: str, interval: P.Interval, settings
    ):
        key = self.get_key(location, step, metric)
        intervals = self.hook.load_intervals(self.config_index, key, self.get_default())

        intervals[interval] = settings

        self.hook.store_intervals(self.config_index, key, intervals)

    def get_transforms(
        self, location: str, step: str, metrics: List[str], interval: P.Interval
    ):
        keys = [self.get_key(location, step, metric) for metric in metrics]
        all_intervals = self.hook.load_multiple_intervals(
            self.config_index, keys, self.get_default()
        )

        return {
            metric: all_intervals[self.get_key(location, step, metric)][interval]
            for metric in metrics
        }

    def set_transforms(
        self,
        location: str,
        step: str,
        metrics: List[str],
        interval: P.Interval,
        settings,
    ):
        keys = [self.get_key(location, step, metric) for metric in metrics]
        all_intervals = self.hook.load_multiple_intervals(
            self.config_index, keys, self.get_default()
        )

        for metric in metrics:
            all_intervals[self.get_key(location, step, metric)][interval] = settings

        self.hook.store_multiple_intervals(self.config_index, all_intervals)

    def get_key(self, location, step, metric):
        return f"transform::{location}::{step}::{metric}"

    def get_default(self):
        return {"transformation": "nop", "transformation_params": {}}
