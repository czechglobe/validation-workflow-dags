import logging
import uuid
from datetime import datetime
from typing import List

import pandas as pd
import portion as P
from werkzeug.utils import cached_property

from airflow import DAG
from airflow.models import DagBag, clear_task_instances
from airflow.utils.db import provide_session
from validation_workflow.metrics_config import MetricConfig
from validation_workflow.utils import ALL_STEPS, idealize


def _datetime_fix_sql(d: datetime):
    return datetime(d.year, d.month, d.day, d.hour, d.minute, d.second, tzinfo=d.tzinfo)


def simplify_airflow_state(state):
    if state == "success":
        return "success"
    elif state == "retry" or state == "up_for_retry":
        return "retry"
    elif state == "failed" or state == "upstream_failed":
        return "failed"
    else:
        return "waiting"


class DAGModel:
    @cached_property
    def dag_bag(self):
        return DagBag()

    @provide_session
    def clear_tasks(
        self, location: str, task_id: str, interval: P.Interval, session=None
    ):
        dag = self.dag_bag.get_dag(location)
        if not dag:
            raise ValueError("DAG not found")

        task = dag.get_task(task_id)
        downstream_tasks = task.get_flat_relatives(upstream=False)
        downstream_tasks = [task] + downstream_tasks

        task_instances_to_clear = [
            task_instance
            for downstream_task in downstream_tasks
            for task_instance in downstream_task.get_task_instances(
                (_datetime_fix_sql(interval.lower) - dag.schedule_interval),
                _datetime_fix_sql(interval.upper),
            )
        ]
        clear_task_instances(task_instances_to_clear, session, True, dag)

    @provide_session
    def clear_and_run_tasks(
        self,
        location: str,
        task_id: str,
        interval: P.Interval,
        interactive: bool = False,
        session=None,
    ):
        dag = self.dag_bag.get_dag(location)
        if not dag:
            raise ValueError("DAG not found")

        task = dag.get_task(task_id)
        downstream_tasks = task.get_flat_relatives(upstream=False)

        if interactive:
            task_instances = task.get_task_instances(
                (_datetime_fix_sql(interval.lower) - dag.schedule_interval),
                _datetime_fix_sql(interval.upper),
            )
            for task_instance in task_instances:
                job_id = f"interactive-{task.task_id}-{task_instance.execution_date.isoformat()}-{uuid.uuid4()}"
                logging.info(f"Interactively executing job {job_id}")
                task_instance.task = task
                task_instance._run_raw_task(test_mode=True, job_id=job_id)
        else:
            downstream_tasks = [task] + downstream_tasks

        task_instances_to_clear = [
            task_instance
            for downstream_task in downstream_tasks
            for task_instance in downstream_task.get_task_instances(
                (_datetime_fix_sql(interval.lower) - dag.schedule_interval),
                _datetime_fix_sql(interval.upper),
            )
        ]
        clear_task_instances(task_instances_to_clear, session, True, dag)

    def get_metric_status(
        self, location: str, interval: P.Interval, metrics: List[MetricConfig]
    ):
        result = {step: P.IntervalDict() for step in ALL_STEPS}

        filter_tasks = {
            step: [
                f"fix_{step}_data_{idealize(getattr(m, f'name_{step}'))}"
                for m in metrics
                if getattr(m, f"name_{step}")
            ]
            for step in ALL_STEPS[:2]
        }
        task_states = {step: [] for step in ALL_STEPS}

        dag: DAG = self.dag_bag.get_dag(location)

        last_task_raw = dag.get_task("transform_to_physical_units")
        tasks_raw = last_task_raw.get_flat_relatives(upstream=True)
        tasks_raw = set(
            filter(
                lambda t: not t.task_id.startswith("fix_raw_data")
                or t.task_id in filter_tasks["raw"],
                tasks_raw,
            )
        )
        task_states["raw"] = [
            (ti.execution_date, ti.state)
            for t in tasks_raw
            for ti in t.get_task_instances(
                (_datetime_fix_sql(interval.lower) - dag.schedule_interval),
                _datetime_fix_sql(interval.upper),
            )
        ]

        last_task_unit = dag.get_task("store_final_data")
        tasks_unit = last_task_unit.get_flat_relatives(upstream=True)
        tasks_unit = set(
            filter(
                lambda t: not t.task_id.startswith("fix_unit_data")
                or t.task_id in filter_tasks["unit"],
                tasks_unit,
            )
        )
        tasks_unit = tasks_unit - tasks_raw
        task_states["unit"] = [
            (ti.execution_date, ti.state)
            for t in tasks_unit
            for ti in t.get_task_instances(
                (_datetime_fix_sql(interval.lower) - dag.schedule_interval),
                _datetime_fix_sql(interval.upper),
            )
        ]

        tasks_final = set(dag.tasks)
        tasks_final = (tasks_final - tasks_unit) - tasks_raw
        task_states["final"] = [
            (ti.execution_date, ti.state)
            for t in tasks_final
            for ti in t.get_task_instances(
                (_datetime_fix_sql(interval.lower) - dag.schedule_interval),
                _datetime_fix_sql(interval.upper),
            )
        ]

        # pessimistic reduction to one interval for each step (failed > retry > waiting > success)
        for step in ALL_STEPS:
            result[step][interval] = "undefined"

            states_series = pd.DataFrame(
                task_states[step], columns=["execution_date", "state"]
            ).set_index("execution_date")["state"]
            states_series = states_series.apply(simplify_airflow_state)

            for state in ["success", "waiting", "retry", "failed"]:
                for execution_date in states_series.index[states_series == state]:
                    task_interval = P.closedopen(
                        execution_date, execution_date + dag.schedule_interval
                    )
                    result[step][task_interval] = state

        return result
