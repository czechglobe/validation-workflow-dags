import click
import pandas as pd

import validation_workflow.scripts.command_fast_backfill as command_fast_backfill
import validation_workflow.scripts.command_setup as command_setup
import validation_workflow.scripts.command_import as command_import
import validation_workflow.scripts.command_export as command_export


@click.group()
def main():
    """
    CLI for validation_workflow management
    """


def parse_date(_ctx, _param, value):
    return pd.to_datetime(value, utc=True)


@main.command(name="setup")
def setup():
    command_setup.setup()


@main.command(name="fast-backfill")
@click.option("-d", "--dag_id", type=str, required=True)
@click.option(
    "-s",
    "--start",
    callback=parse_date,
    help="Start of backfill interval",
    required=True,
)
@click.option(
    "-e",
    "--end",
    callback=parse_date,
    help="End of backfill interval (inclusive)",
    required=True,
)
def fast_backfill(dag_id, start, end):
    command_fast_backfill.fast_backfill(dag_id, start, end)


@main.command(name="import")
@click.argument("path", type=click.Path(exists=True), required=True)
@click.option("-l", "--location", type=str, required=True)
@click.option("-h", "--header_row", type=int, required=True)
def import_any(path, location, header_row):
    command_import.import_guess_type(path, location, header_row)


@main.command(name="export")
@click.argument("path", type=click.Path(exists=False), required=True)
@click.option("-l", "--location", type=str, required=True)
@click.option(
    "-s", "--start", callback=parse_date, help="Start of export interval", required=True
)
@click.option(
    "-e",
    "--end",
    callback=parse_date,
    help="End of export interval (inclusive)",
    required=True,
)
@click.option(
    "--step",
    type=str,
    help="Step/index name",
    required=False,
    default="final",
    show_default=True,
)
@click.option(
    "-q",
    "--qcodes",
    type=bool,
    help="Include metrics qcodes",
    required=False,
    default=True,
    show_default=True,
)
def export_any(path, location, start, end, step, qcodes):
    command_export.export_guess_type(path, location, start, end, step, qcodes)


if __name__ == "__main__":
    main()
