import logging
import os

from airflow import settings as airflow_settings
from airflow.models import Connection
from airflow.utils.db import merge_conn
from validation_workflow import settings
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook


def create_airflow_connections():
    """
    Create default Airflow connections.
    """
    session = airflow_settings.Session()

    airflow_db_conn = (
        session.query(Connection).filter(Connection.conn_id == "airflow_db").first()
    )
    if airflow_db_conn:
        airflow_db_conn.conn_type = "postgres"
        airflow_db_conn.host = os.getenv("POSTGRES_HOST")
        airflow_db_conn.port = 5432
        airflow_db_conn.login = os.getenv("POSTGRES_USER")
        airflow_db_conn.password = os.getenv("POSTGRES_PASSWORD")
        airflow_db_conn.schema = os.getenv("POSTGRES_DB")
        session.commit()

    merge_conn(
        Connection(
            conn_id="input_fs", conn_type="fs", extra='{"path": "/var/data-input"}'
        ),
        session,
    )
    merge_conn(
        Connection(
            conn_id=settings.conn_id_es,
            conn_type="http",
            host="elasticsearch",
            port=9200,
        ),
        session,
    )


def create_es_indices():
    hook = ElasticsearchHook(settings.conn_id_es)

    hook.create_index(
        settings.index_intervals_config,
        {
            "mappings": {
                "properties": {
                    "fragments": {"type": "object", "enabled": False},
                    "key": {"type": "text", "analyzer": "keyword"},
                }
            }
        },
    )

    for step_index in ["raw", "raw_fixed", "unit", "unit_fixed", "final"]:
        hook.create_index(
            step_index,
            {
                "mappings": {
                    "properties": {
                        "@timestamp": {"type": "date"},
                        "location": {
                            "type": "text",
                            "fields": {
                                "keyword": {"type": "keyword", "ignore_above": 256}
                            },
                        },
                        "qcode": {"type": "integer"},
                    }
                }
            },
        )


def setup():
    logging.info("Creating airflow connections")
    create_airflow_connections()
    logging.info("Creating elasticsearch indices")
    create_es_indices()
    logging.info("Done")
