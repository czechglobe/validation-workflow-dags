from click import ClickException

from validation_workflow.metrics_config import load_metrics_config

metrics_config = load_metrics_config()


def get_location_config(location):
    if location in metrics_config.get_locations():
        return metrics_config.get_location_config(location)
    else:
        raise ClickException(
            f"Location {location} is not valid. List of valid locations: {', '.join(metrics_config.get_locations())}"
        )
