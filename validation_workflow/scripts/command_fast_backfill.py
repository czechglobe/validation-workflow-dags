import copy
import logging
from datetime import datetime, timedelta
from typing import List

import pandas as pd
import pendulum
from airflow.utils.state import State

from airflow import DAG
from airflow.models import DagBag, TaskInstance, DagRun
from airflow.utils.db import provide_session


def get_dag(dag_id: str) -> DAG:
    dag_bag = DagBag()
    return dag_bag.get_dag(dag_id)


@provide_session
def refresh_dagruns(dag, start_date, execution_dates, session=None):
    for ed in execution_dates:
        ped: datetime = pendulum.parse(ed.isoformat())
        run: DagRun = dag.get_dagrun(ped, session=session)
        if run:
            run.set_state(State.SUCCESS)
            session.merge(run)
        else:
            run = dag.create_dagrun(
                run_id=f"fast_backfill_{ped.isoformat()}",
                state=State.SUCCESS,
                execution_date=ped,
                start_date=start_date,
                session=session,
            )
        tis: List[TaskInstance] = run.get_task_instances(session=session)
        for ti in tis:
            ti.set_state(State.SUCCESS, commit=False)
    session.commit()


def fast_backfill(dag_id, start, end):
    start_date = pendulum.now()

    # disable not-important logging
    logging.getLogger("elasticsearch").setLevel(logging.WARNING)
    logging.getLogger("airflow").setLevel(logging.WARNING)

    dag = get_dag(dag_id)
    sorted_tasks = dag.topological_sort()

    assert isinstance(dag.schedule_interval, timedelta)

    large_dag = copy.deepcopy(dag)
    large_dag.schedule_interval = (end + dag.schedule_interval) - start
    # TODO can be fixed or removed for higher airflow versions
    # pylint: disable=protected-access
    large_dag._schedule_interval = large_dag.schedule_interval

    execution_dates = pd.date_range(start, end, freq=dag.schedule_interval)

    for task in sorted_tasks:
        mark_success = task.task_id.endswith("_sensor")

        allow_large_tasks = (
            task.task_id.startswith("fix_raw_data_")
            or task.task_id == "transform_to_physical_units"
            or task.task_id.startswith("fix_unit_data_")
            or task.task_id == "store_final_data"
        )

        if allow_large_tasks:
            large_task = large_dag.get_task(task.task_id)
            ped = pendulum.parse(start.isoformat())
            ti = TaskInstance(large_task, execution_date=ped)
            logging.info("Executing single huge task %s", task.task_id)
            # pylint: disable=protected-access
            ti._run_raw_task(test_mode=True)
        else:
            for ed in execution_dates:
                ped = pendulum.parse(ed.isoformat())
                ti = TaskInstance(task, execution_date=ped)
                logging.info("Executing task %s at %s", task.task_id, ed.isoformat())
                # pylint: disable=protected-access
                ti._run_raw_task(mark_success=mark_success)

    logging.info("Marking dagruns as success")
    refresh_dagruns(dag, start_date, execution_dates)

    logging.info("Done")
