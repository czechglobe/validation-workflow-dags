import logging
import os

import pandas as pd
from click import ClickException
from dateutil.parser._parser import ParserError

from validation_workflow.scripts.utils import get_location_config
from validation_workflow.utils import fix_input_series
from validation_workflow import settings
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook


def import_excel(path, location, header_row):
    header_row = header_row - 1

    logging.info("Loading excel file..")

    df = pd.read_excel(path, header=header_row, index_col=0)

    logging.info("Finding valid columns..")

    location_config = get_location_config(location)
    metric_names = [
        metric.name_raw for metric in location_config.metrics if metric.name_raw
    ]

    def fix_column_name(col):
        if col in metric_names:
            return col
        return None

    fixed_columns = [fix_column_name(col) for index, col in enumerate(df.columns)]
    invalid_columns = df.columns[[col is None for col in fixed_columns]]
    fixed_columns = [col for col in fixed_columns if col is not None]

    if not len(fixed_columns):
        logging.warning("No valid columns found! Read columns: %s", ",".join(df.columns))
        return

    logging.info("Found %s columns: %s", len(fixed_columns), ", ".join(fixed_columns))

    df.drop(columns=invalid_columns, inplace=True)
    df.columns = fixed_columns

    def try_to_datetime(date):
        try:
            dt = pd.to_datetime(date)
            if not pd.isnull(dt):
                return dt
        except ParserError:
            pass
        return None

    new_index = [try_to_datetime(date) for date in df.index]
    invalid_indices = df.index[[date is None for date in new_index]]
    df.drop(index=invalid_indices, inplace=True)
    df.index.name = "@timestamp"

    # keep first row in case of duplicated index
    df = df[~df.index.duplicated(keep='first')]

    if df.shape[0] == 0:
        logging.warning("No data found!")
        return
    logging.info("Found data of size %s, %s", *df.shape)

    es_hook = ElasticsearchHook(settings.conn_id_es)

    for col in df.columns:
        logging.info("Storing time series %s", col)
        time_series = pd.DataFrame(columns=["value", "qcode"])
        time_series["value"] = fix_input_series(df[col])
        time_series["qcode"] = 0
        es_hook.store_time_series("raw", location, col, time_series)


def import_guess_type(path, *args, **kwargs):
    _file_name, extension = os.path.splitext(path)
    if extension in [".xls", ".xlsx", ".xlsm", ".xlsb", ".odf", ".ods", ".odt"]:
        import_excel(path, *args, **kwargs)
    else:
        raise ClickException(f"Unsupported file type {extension}")
