import os
from datetime import datetime

import pandas as pd
from click import ClickException

import validation_workflow.settings as settings
from validation_workflow.hooks.elasticsearch_hook import ElasticsearchHook
from validation_workflow.scripts.utils import get_location_config
from validation_workflow.utils import get_timestamps
from validation_workflow.metrics_config import load_metrics_config

metrics_config = load_metrics_config()


def get_export_df(
    location: str,
    start: datetime,
    end: datetime,
    step: str = "final",
    qcodes: bool = False,
) -> pd.DataFrame:
    es_hook = ElasticsearchHook(settings.conn_id_es)

    location_config = get_location_config(location)
    metric_names = [
        metric.name_raw for metric in location_config.metrics if metric.name_raw
    ]

    timestamps = get_timestamps(start, end, location_config.frequency)

    df = es_hook.load_fields_df(step, location, timestamps, metric_names, qcodes)

    return df


def export_excel(path: str, *args, **kwargs):
    df = get_export_df(*args, **kwargs)

    # excel does not support timezones
    df.index = pd.Series(df.index.values).dt.tz_localize(None)

    df.to_excel(path)


def export_csv(path: str, *args, **kwargs):
    df = get_export_df(*args, **kwargs)
    df.to_csv(path)


def export_guess_type(path, *args, **kwargs):
    _file_name, extension = os.path.splitext(path)
    if extension == ".csv":
        export_csv(path, *args, **kwargs)
    elif extension in [".xls", ".xlsx", ".xlsm", ".xlsb", ".odf", ".ods", ".odt"]:
        export_excel(path, *args, **kwargs)
    else:
        raise ClickException(f"Unsupported file type {extension}")
