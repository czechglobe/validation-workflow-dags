# validation-workflow-api

Part of Validation Workflow system (https://gitlab.com/czechglobe/validation-workflow). This submodule contains airflow DAGs, operators and data transformations.
