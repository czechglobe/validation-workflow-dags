ARG AIRFLOW_VERSION
FROM puckel/docker-airflow:${AIRFLOW_VERSION}

# required by airflow password authentication, but it is missing in puckel/docker-airflow image
RUN pip install flask_bcrypt

COPY . ${AIRFLOW_HOME}/validation-workflow-dags

USER root

RUN pip install -r ${AIRFLOW_HOME}/validation-workflow-dags/all-requirements.txt

RUN mkdir /var/data-input && chown airflow /var/data-input

USER airflow

WORKDIR ${AIRFLOW_HOME}/validation-workflow-dags
ENV PYTHONPATH=${AIRFLOW_HOME}/validation-workflow-dags
ENV AIRFLOW__CORE__DAGS_FOLDER=${AIRFLOW_HOME}/validation-workflow-dags/validation_workflow/dags

ENTRYPOINT ["/entrypoint.sh"]
